import React from "react";
import MainLayout from "../../layout/mainLayout";
import WrapperCard from "../../components/wrapperCard/wrapperCard";
import {
  makeStyles,
  Card,
  Grid,
  Button,
  TextField,
  Select,
  MenuItem,
  FormLabel,
  TextareaAutosize,
  Input,
  FormControlLabel,
  Radio,
  RadioGroup,
  createMuiTheme,
  Box,
} from "@material-ui/core";
import { useNavigate } from "react-router-dom";
import { fontSize } from "@mui/system";
const useStyles = makeStyles((theme) => ({
  card: {
    maxWidth: "100%",
    boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
    minHeight: "100vh",
    margin: "auto",
    marginTop: "100px",
    borderRadius: "5px",
    padding: "20px",
  },
}));

export default function Dashboard() {
  const classes = useStyles();
  let navigate = useNavigate();
  const handleChange= (name) => {
    if(name==="store"){
        navigate(`/create-store/add-store`);
    }
    else if(name==="bulkinvite"){
        navigate(`/bulkinvite`);
    }
    
  };
  return (
    <MainLayout>
      <Card variant="outlined" className={classes.card}>
        <Grid container spacing={2} columns={{ xs: 2, md: 12 }}>
          <Grid item xs={12} md={6}>
            <WrapperCard bg="#909296">
              <Box
              
                sx={{
                  textAlign: "center",
                  padding: "10px",
                  color: "white",
                  fontSize: "1.5em",
                }}
               
              >
                Add store
              </Box>
            </WrapperCard>
          </Grid>

          <Grid item xs={12} md={6}>
            <WrapperCard bg="#909296">
              <Box
                sx={{
                  textAlign: "center",
                  padding: "10px",
                  color: "white",
                  fontSize: "1.5em",
                }}
                
              >
                Add Franchisee
              </Box>
            </WrapperCard>
          </Grid>

          <Grid item xs={12} md={6}>
            <WrapperCard bg="#2a4987">
              <Box
                sx={{
                  textAlign: "center",
                  padding: "10px",
                  color: "white",
                  fontSize: "1.5em",
                }}
                onClick={()=>handleChange("store")}
              >
                One by one
              </Box>
            </WrapperCard>
          </Grid>

          <Grid item xs={12} md={6}>
            <WrapperCard bg="#2a4987">
              <Box
                sx={{
                  textAlign: "center",
                  padding: "10px",
                  color: "white",
                  fontSize: "1.5em",
                }}
               
              >
                Bulk Invite
              </Box>
            </WrapperCard>
          </Grid>
          <Grid item xs={12} md={6}>
            <WrapperCard bg="#2a4987">
              <Box
                sx={{
                  textAlign: "center",
                  padding: "10px",
                  color: "white",
                  fontSize: "1.5em",
                }}
                onClick={()=>handleChange("bulkinvite")}
              >
                Bulk Invite
              </Box>
            </WrapperCard>
          </Grid>
        </Grid>
      </Card>
    </MainLayout>
  );
}
