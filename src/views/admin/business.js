import React from 'react';
import MainLayout from "../../layout/mainLayout";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import CardBusinessDetails from "../../components/Cards/CardBusinessDetails";
import CardBankDetails from "../../components/Cards/CardBankDetails";
import CardShareCoupons from '../../components/Cards/CardCampaigns';

import {
  makeStyles,
 
  Grid,
  Box,
  IconButton,

  TextField,
  Select,
  MenuItem,
  FormLabel,
  TextareaAutosize,
  Input,
  FormControlLabel,
  Radio,
  RadioGroup,
  createMuiTheme,
} from "@material-ui/core";
import BackupIcon from "@mui/icons-material/Backup";

const theme = createMuiTheme({
  overrides: {
    MuiInput: {
      underline: {
        "&:hover:not($disabled):before": {
          backgroundColor: "rgba(0, 188, 212, 0.7)",
        },
      },
    },
  },
});

const useStyles = makeStyles((theme) => ({
  lableTitle: {
    fontSize: "0.75rem",
    lineHeight: "1rem",
    fontWeight: "bold",
    color: "#57748a",
  },
  lable: {
    fontSize: "0.75rem",
    lineHeight: "1rem",
    fontWeight: "bold",
    color: "#57748a",
  },
  cards: {
    marginTop: "100px",
  },

  card: {
    boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
    borderRadius: "5px",
    padding: "20px",
  },
  canc: {
    background: "blue",
    color: "white",
    fontWeight: "700",
    "&:hover": {
      background: "blue",
      color: "white",
    },
  },
  inputfile: {
    display: "none",
  },
  upload: {
    background: "#f0f3f5",
    display: "flex",
    flexDirection: "column",
    width: "100%",
    height: "100%",
    justifyContent: "center",
  },
  radio: {
    "&$checked": {
      color: "#4B8DF8",
    },
  },
  checked: {},
}));
export default function Business() {
      const classes = useStyles();

  return (
    <>
      <MainLayout>
        <Grid container spacing={2} className={classes.cards}>
          <Grid
            item
            xs={12}
            md={6}
            sx={{
              boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
              borderRadius: "5px",
              padding: "20px",
            }}
          >
            <CardBusinessDetails />
          
          </Grid>
          <Grid item xs={12} md={6}>
            <CardBankDetails/>
          </Grid>
        </Grid>
      
      </MainLayout>
    </>
  );
}

