import React from "react";
import MainLayout from "../../layout/mainLayout";
import {
  makeStyles,
  Card,
  Grid,
  Button,
  Box,
  IconButton,
  TextField,
  Select,
  MenuItem,
  FormLabel,
  TextareaAutosize,
  Input,
  FormControlLabel,
  Radio,
  RadioGroup,
  createMuiTheme,
} from "@material-ui/core";
import Autocomplete from "@mui/lab/Autocomplete";
import Paper from "@mui/material/Paper";
import {
  claimCoupon
} from "../../service config/admin.service";
import CardShareCoupons from "../../components/Cards/CardCampaigns";
import { padding, textAlign } from "@mui/system";
import { ToastContainer, toast } from "react-toastify";


const useStyles = makeStyles((theme) => ({
  cards: {
    marginTop: "100px",
  },
  card: {
    // padding: "100px",
    height:"50vh",
    display:"flex",
    alignItems:"center",
    justifyContent:"center"
  },
  btn: {
    background: "blue",
    color: "white",
    fontWeight: "700",
    "&:hover": {
      background: "blue",
      color: "white",
    },
  },
  form:{
    width:"100%",
  }
}));

export default function ClaimCoupon() {
  const classes = useStyles();
  const [coupon,setCoupon]=React.useState("")
  const [inputValue,setInputValue]=React.useState("")

  const top100Films = [
    { title: "The Shawshank Redemption", year: 1994 },
    { title: "The Godfather", year: 1972 },
    { title: "The Godfather: Part II", year: 1974 },
    { title: "The Dark Knight", year: 2008 },
    { title: "12 Angry Men", year: 1957 },
    { title: "Schindler's List", year: 1993 },
    { title: "Pulp Fiction", year: 1994 },
    { title: "The Lord of the Rings: The Return of the King", year: 2003 },
    { title: "The Good, the Bad and the Ugly", year: 1966 },
    { title: "Fight Club", year: 1999 },
    { title: "The Lord of the Rings: The Fellowship of the Ring", year: 2001 },
    { title: "Star Wars: Episode V - The Empire Strikes Back", year: 1980 },
    { title: "Forrest Gump", year: 1994 },
    { title: "Inception", year: 2010 },
    { title: "The Lord of the Rings: The Two Towers", year: 2002 },
    { title: "One Flew Over the Cuckoo's Nest", year: 1975 },
    { title: "Goodfellas", year: 1990 },
    { title: "The Matrix", year: 1999 },
    { title: "Seven Samurai", year: 1954 },
    { title: "Star Wars: Episode IV - A New Hope", year: 1977 },
    { title: "City of God", year: 2002 },
    { title: "Se7en", year: 1995 },
    { title: "The Silence of the Lambs", year: 1991 },
    { title: "It's a Wonderful Life", year: 1946 },
    { title: "Life Is Beautiful", year: 1997 },
    { title: "The Usual Suspects", year: 1995 },
    { title: "Léon: The Professional", year: 1994 },
    { title: "Spirited Away", year: 2001 },
    { title: "Saving Private Ryan", year: 1998 },
    { title: "Once Upon a Time in the West", year: 1968 },
    { title: "American History X", year: 1998 },
    { title: "Interstellar", year: 2014 },
    { title: "Casablanca", year: 1942 },
    { title: "City Lights", year: 1931 },
    { title: "Psycho", year: 1960 },
    { title: "The Green Mile", year: 1999 },
    { title: "The Intouchables", year: 2011 },
    { title: "Modern Times", year: 1936 },
    { title: "Raiders of the Lost Ark", year: 1981 },
    { title: "Rear Window", year: 1954 },
    { title: "The Pianist", year: 2002 },
    { title: "The Departed", year: 2006 },
    { title: "Terminator 2: Judgment Day", year: 1991 },
    { title: "Back to the Future", year: 1985 },
    { title: "Whiplash", year: 2014 },
    { title: "Gladiator", year: 2000 },
    { title: "Memento", year: 2000 },
    { title: "The Prestige", year: 2006 },
    { title: "The Lion King", year: 1994 },
    { title: "Apocalypse Now", year: 1979 },
    { title: "Alien", year: 1979 },
    { title: "Sunset Boulevard", year: 1950 },
    {
      title:
        "Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb",
      year: 1964,
    },
    { title: "The Great Dictator", year: 1940 },
    { title: "Cinema Paradiso", year: 1988 },
    { title: "The Lives of Others", year: 2006 },
    { title: "Grave of the Fireflies", year: 1988 },
    { title: "Paths of Glory", year: 1957 },
    { title: "Django Unchained", year: 2012 },
    { title: "The Shining", year: 1980 },
    { title: "WALL·E", year: 2008 },
    { title: "American Beauty", year: 1999 },
    { title: "The Dark Knight Rises", year: 2012 },
    { title: "Princess Mononoke", year: 1997 },
    { title: "Aliens", year: 1986 },
    { title: "Oldboy", year: 2003 },
    { title: "Once Upon a Time in America", year: 1984 },
    { title: "Witness for the Prosecution", year: 1957 },
    { title: "Das Boot", year: 1981 },
    { title: "Citizen Kane", year: 1941 },
    { title: "North by Northwest", year: 1959 },
    { title: "Vertigo", year: 1958 },
    { title: "Star Wars: Episode VI - Return of the Jedi", year: 1983 },
    { title: "Reservoir Dogs", year: 1992 },
    { title: "Braveheart", year: 1995 },
    { title: "M", year: 1931 },
    { title: "Requiem for a Dream", year: 2000 },
    { title: "Amélie", year: 2001 },
    { title: "A Clockwork Orange", year: 1971 },
    { title: "Like Stars on Earth", year: 2007 },
    { title: "Taxi Driver", year: 1976 },
    { title: "Lawrence of Arabia", year: 1962 },
    { title: "Double Indemnity", year: 1944 },
    { title: "Eternal Sunshine of the Spotless Mind", year: 2004 },
    { title: "Amadeus", year: 1984 },
    { title: "To Kill a Mockingbird", year: 1962 },
    { title: "Toy Story 3", year: 2010 },
    { title: "Logan", year: 2017 },
    { title: "Full Metal Jacket", year: 1987 },
    { title: "Dangal", year: 2016 },
    { title: "The Sting", year: 1973 },
    { title: "2001: A Space Odyssey", year: 1968 },
    { title: "Singin' in the Rain", year: 1952 },
    { title: "Toy Story", year: 1995 },
    { title: "Bicycle Thieves", year: 1948 },
    { title: "The Kid", year: 1921 },
    { title: "Inglourious Basterds", year: 2009 },
    { title: "Snatch", year: 2000 },
    { title: "3 Idiots", year: 2009 },
    { title: "Monty Python and the Holy Grail", year: 1975 },
  ];
  
  const handleSubmit = async(e)=>{
    e.preventDefault()
  const res= await claimCoupon({code:coupon})
  console.log(res)
  
  if (!res.data.error) {
    let msg = {err: false, msg: ""};
  if(res.status == 200){
      msg.msg = "Coupon has been claimed successfully"
  }else if(res.status == 409){
      msg.err = true;
      msg.msg =  "Coupon has been claimed already";
  }else if(res.status == 500){
      msg.err = true;
      msg.msg = "please try after some time";
  }else {
      msg.err = true;
      msg.msg = 'Enter a valid code'
  }
  if(msg.err) toast.error(msg.msg)
  if(!msg.err) toast.success(msg.msg)
  } else {
    toast.error(res.data.payload);

  }
    

  }

  return (
    <>
      <MainLayout>
        <div className={classes.cards}>
          <Card variant="outlined" className={classes.card}>
            <form onSubmit={handleSubmit} className={classes.form}>
            <Grid container spacing={{xs: 3, sm: 4, md: 3}} alignItems='center'
    justify='center' align="center">
              <Grid item xs={12} md={6} lg={4}   >
                <Box sx={{margin:"10px"}}>
                    <TextField
                      label="Find Coupons"
                      variant="outlined"
                      type="search"
                      onChange={(e)=>setCoupon(e.target.value)}
                     
                    />
                    </Box>
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <Button
                  variant="contained"
                  size="large"
                  className={classes.btn}
                  type="submit"
                >
                  Claim Coupon
                </Button>
              </Grid>
            </Grid>
            {/* <Paper
              sx={{
                background: "green",
                color: "white",
                textAlign: "center",
                marginTop: "70px",
                padding: "10px",
                borderRadius: "70px",
              }}
            >
              Coupon A45RER is expired
            </Paper> */}
            </form>
          </Card>
        </div>
      </MainLayout>
      {/* <ToastContainer/> */}
    </>
  );
}
