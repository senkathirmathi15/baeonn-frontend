import * as React from "react";
import PropTypes from "prop-types";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import {Link } from "react-router-dom"
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import TableSelect from "../../components/table/selectTable";
import { useSelector, useDispatch } from "react-redux";
import { storeReducer } from "../../service config/configStore";
import FileUploadHoc from "../../components/File.Upload";
import { ToastContainer, toast } from "react-toastify";
import Chip from "@mui/material/Chip";
import MuiPhoneNumber from "material-ui-phone-number";
import Lable from "../../components/Typography/lable"
import CompanyLogo from "../../assets/ill_header.png"
import { getCurrentUser } from "../../service config/auth.service";


import {
  submitStoreDetail,
  updateStoreSubmit,
  getStoreDetail,
  getInviteStore,
  reSendInviteStore,
  deleteSendInviteStore,
  getStoreDetailforManager
} from "../../service config/admin.service";

import {
  makeStyles,
  Card,
  Grid,
  Button,
  TextField,
  Select,
  MenuItem,
  FormLabel,
  TextareaAutosize,
  Input,
  FormControlLabel,
  Radio,
  RadioGroup,
  Switch,
  createMuiTheme,
   
} from "@material-ui/core";
import PhotoCamera from "@mui/icons-material/PhotoCamera";
import BackupIcon from "@mui/icons-material/Backup";
import HeightIcon from "@mui/icons-material/Height";
import MainLayout from "../../layout/mainLayout";
import WrapperCard from "../../components/wrapperCard/wrapperCard";
import {STOREMANAGER,STOREOWNER} from "../../utilities/constants"
import Stack from "@mui/material/Stack";
import { getListItemSecondaryActionClassesUtilityClass } from "@mui/material";

import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';

const drawerWidth = 270;
const theme = createMuiTheme({
  overrides: {
    MuiInput: {
      underline: {
        "&:hover:not($disabled):before": {
          backgroundColor: "rgba(0, 188, 212, 0.7)",
        },
      },
    },
  },
});
function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData("Frozen yoghurt", 159, 6.0, 24, 4.0),
  createData("Ice cream sandwich", 237, 9.0, 37, 4.3),
  createData("Eclair", 262, 16.0, 24, 6.0),
  createData("Cupcake", 305, 3.7, 67, 4.3),
  createData("Gingerbread", 356, 16.0, 49, 3.9),
];

const useStyles = makeStyles((theme) => ({
  lable: {
    fontSize: "0.75rem",
    lineHeight: "1rem",
    fontWeight: "bold",
    color: "#57748a",
  },
  card: {
    maxWidth: "1000px",
    boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
    // minHeight: "100vh",
    margin: "0px auto",
    marginTop: "100px",
    borderRadius: "5px",
    padding: "20px",
  },
  listcard: {
    maxWidth: "1000px",
    boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
    // minHeight: "100vh",
    background:"#fbfcfa",
    margin: "0px auto",
    marginTop: "10px",
    borderRadius: "20px",
    
  },
  cancel: {
    background: "blue",
    color: "white",
    fontWeight: "700",
    "&:hover": {
      background: "blue",
      color: "white",
    },
  },
  inputfile: {
    display: "none",
  },
  upload: {
    background: "#f0f3f5",
    display: "flex",
    flexDirection: "column",
    width: "100%",
    height: "100%",
    justifyContent: "center",
  },
  icon: {
    transform: "rotate(180deg)",
  },
  grid: {
    padding: "25px 0",
  },
  input: {
    border: "none",
    boxShadow: "0 3px 5px rgb(0 0 0 / 0.1)",
    background: "white",
    "&:hover": {
      border: "none",
      outline: "none",
    },
  },
  radio: {
    "&$checked": {
      color: "#4B8DF8",
    },
  },
  flex:{
    display:"flex",
    justifyContent:"space-between",
    flex: "wrap",
    [theme.breakpoints.down('sm')]: {
      flexDirection:"column"
    },
  },
  checked: {},
  edit: {},
  companyLogo: {
    width: "100%",
    height: "100%",
    objectFit: "cover",
    borderRadius: "50%",
  },
}));

const states = ["Tamil Nadu"];
const cities = [
  "Chennai",
  "Coimbatore",
  "Madurai",
  "Tiruchirappalli",
  "Salem",
  "Tirunelveli",
  "Tiruppur",
  "Vellore",
  "Erode",
  "Thoothukkudi",
  "Dindigul",
  "Thanjavur",
  "Ranipet",
  "Sivakasi",
  "Karur",
  "Udhagamandalam",
  "Hosur",
  "Nagercoil",
  "Kanchipuram",
  "Kumarapalayam",
  "Karaikkudi",
  "Neyveli",
  "Cuddalore",
  "Kumbakonam",
  "Tiruvannamalai",
  "Pollachi",
  "Rajapalayam",
  "Gudiyatham",
  "Pudukkottai",
  "Vaniyambadi",
  "Ambur",
  "Nagapattinam",
];
const categories = [
  "Supermarket/Hypermarket/Grocery Store",
  "Restaurant/Food & Beverage/Canteen",
  "Bakery/Ice cream parlor/Cafe ",
  "Pharmacies/Wellness Stores",
  "Fitness centre/yoga centre",
  "Mobile stores",
  "Beauty parlour/Saloon/Spa",
  "Footwear stores",
  "Textile/Boutique/Tailor shop",
  "Fancy Stores ",
  "Jewelry shop",
  "Opticals",
  "Hotels/Lodges ",
  "Cinema Theatre",
  "Furnitures/Home Appliances",
  "Auto spares and electricals/Hardwares",
  "Fireworks/Crackers shop",
];

function Store(props) {
  const [value, setValue] = React.useState('1');
  const [deleted, setDeleted] = React.useState(false);

  const handleChange = (event,newValue) => {
    setValue(newValue);
  };
  const stores = useSelector((state) => state.storeDetail.stores);
  const invites = useSelector((state) => state.storeDetail.invites);
  const classes = useStyles();
     
  React.useEffect(() => {
    if(getCurrentUser().roleId===STOREOWNER){
      getStoreDetail();
    }
    else{
      getStoreDetailforManager({id:getCurrentUser().storeId});
    }
  
    getInviteStore();
  }, [deleted]);
  


  const handleAction = async (name, value) => {
 
    switch (name) {
      case "delete":
        const res = await deleteSendInviteStore({
          body:{
            invitationIds:[value]
          },
        });
      
        if (!res.error) {
          toast.success(res.msg);
          setDeleted(!deleted)
        } else {
          toast.error(res.msg);
        }

        break;
      case "reSend":
        const response = await reSendInviteStore({
          body:{
            emails:[value]
          },
        });
        
      
        if (!response.error) {
          toast.success(response.msg);
        } else {
          toast.error(response.msg);
        }
        break;
      default:
        break;
    }
  };
  return (
    <MainLayout>
      <Card variant="outlined" className={classes.card}>
      <Box sx={{ width: '100%', typography: 'body1' }}>
      <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <TabList onChange={handleChange} aria-label="lab API tabs example">
            <Tab label="STORES" value="1" />
            {getCurrentUser().roleId === STOREOWNER && <Tab label="INVITES" value="2" />}
            
          </TabList>
        </Box>
        <TabPanel value="1">
          <>
        <Typography
                variant="h6"
                display="block"
                sx={{
                  // fontSize: "12px",
                  lineHeight: "1rem",
                  fontWeight: "bold",
                  color: "#1c1c1c",
                }}
                className={classes.lable}
              >
                STORE DETAILS
              </Typography>
          
          { stores?.map((store)=>
          
          <Card variant="outlined" className={classes.listcard}>
            <Box sx={{textAlign:"end",paddingRight:"10px",paddingTop:"10px"}}>
          <Link to={'/create-store/add-store'} state={{ id:store.id,
          
          isEdit:(getCurrentUser().roleId === STOREMANAGER && store.outletName===""?true:false)
        }}><Button variant="contained" size="small" className={classes.cancel}>VIEW & EDIT</Button></Link>

              </Box>
          <Grid
            container
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
            className={classes.grid}
          >
            <Box sx={{ width: "100px",height: "100px",}}>
            <img src={CompanyLogo} alt="logo" className={classes.companyLogo}/>
            </Box>
           <Box sx={{

            display:"flex",
            // justifyContent:"space-between",
            flexDirection:"column",
            padding:"10px"
           }}>

           <Box  sx={{
                  fontSize: "12px",
                  lineHeight: "0.5rem",
                  fontWeight: "bold",
                  color: "#1c1c1c",
                  textTransform:"uppercase"

                }}>
            
               OUTLET NAME: {store.outletName}
              </Box>

              <Box sx={{
                  // fontSize: "12px",
                  fontSize: "12px",
                  lineHeight: "0.5rem",
                  fontWeight: "bold",
                  color: "#1c1c1c",
                  marginTop:"10px"
                  
                }}>
                 
          EMAIL: {store.email}
             
              </Box>
           </Box>

          </Grid>
          </Card>)}
          </>
        </TabPanel>
        <TabPanel value="2">

        <Typography
                variant="h6"
                display="block"
                sx={{
                  // fontSize: "12px",
                  lineHeight: "1rem",
                  fontWeight: "bold",
                  color: "#1c1c1c",
                  marginBottom:"30px"
                }}
                className={classes.lable}
              >
              INVITED STORES
              </Typography>
              <Stack spacing={2}>
          {invites?.map((item)=><WrapperCard >
            
            <Box className={classes.flex} >
              <Box  sx={{
                  // fontSize: "12px",
                  lineHeight: "1rem",
                  fontWeight: "bold",
                  color: "#1c1c1c",

                }}>
            
               {item?.managerEmail}
            
              </Box>
              <Box sx={{
                  // fontSize: "12px",
                   padding:"10px"
                  
                }}>
             {!item.isInvitationAccepted && <Chip label="PENDING"  color="warning"/>}
             {item.isInvitationAccepted && <Chip label="ACCEPTED"  color="primary"/>}
              </Box>
              

            </Box>
            <Box sx={{textAlign:"end"}}>
            {!item.isInvitationAccepted && <Button variant="contained" size="small" className={classes.cancel} onClick={()=>handleAction("reSend", item?.managerEmail)}>Resend</Button>}
           

              </Box>
            <Box sx={{textAlign:"end",marginTop:"10px"}}>
            {!item.isInvitationAccepted && <Button variant="contained" size="small" className={classes.cancel} onClick={()=>handleAction("delete",item?.id)}>Delete</Button>}           

              </Box>
          </WrapperCard>)}
          </Stack>
        </TabPanel>
        
      </TabContext>
    </Box>
     
         {/* <Stack spacing={2}>
          <WrapperCard>
            <Box className={classes.flex}>
              <Box  sx={{
                  // fontSize: "12px",
                  lineHeight: "1rem",
                  fontWeight: "bold",
                  color: "#1c1c1c",

                }}>
            
                Baeonn@gmail.com
              </Box>
              <Box sx={{
                  // fontSize: "12px",
                   padding:"10px"
                  
                }}>
              <Chip label="pending"  color="warning"/>
              </Box>
              

            </Box>
            <Box sx={{textAlign:"end"}}>
            <Button variant="contained" size="small" className={classes.cancel}>Resend</Button>

              </Box>
          </WrapperCard>
          <WrapperCard>
            <Box className={classes.flex}>
              <Box  sx={{
                  // fontSize: "12px",
                  lineHeight: "1rem",
                  fontWeight: "bold",
                  color: "#1c1c1c",
                }}>
            
                Baeonn@gmail.com
              </Box>
              <Box sx={{
                  // fontSize: "12px",
                   padding:"10px"
                  
                }}>
              <Chip label="Accepted"  color="primary"/>
             
              </Box>
            </Box>
            <Grid container>
                <Grid item xs={12} md={6}>
                Branch name:
                </Grid>
                <Grid item xs={12} md={6}>
                BAEONN fee:
                </Grid>
                <Grid item xs={12} md={6}>
                Sharing Rate:
                </Grid>
                <Grid item xs={12} md={6}>
                Conversion Rate:
                </Grid>
              </Grid>
              <Box sx={{textAlign:"end"}}>
                Edit
              <Switch
              color="primary"
  checked={true}
  // onChange={}
  inputProps={{ 'aria-label': 'controlled' }}
/>
</Box>
          </WrapperCard>
          </Stack> */}
          
        
       
      </Card>
     
          {/* { stores?.map((store)=>
          
          <Card variant="outlined" className={classes.listcard}>
            <Box sx={{textAlign:"end",paddingRight:"10px",paddingTop:"10px"}}>
          <Link to={'/create-store/add-store'} state={{ id:store.id,
          
          isEdit:(getCurrentUser().roleId === STOREMANAGER && store.outletName===""?true:false)
        }}><Button variant="contained" size="small" className={classes.cancel}>VIEW & EDIT</Button></Link>

              </Box>
          <Grid
            container
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
            className={classes.grid}
          >
            <Box sx={{ width: "100px",height: "100px",}}>
            <img src={CompanyLogo} alt="logo" className={classes.companyLogo}/>
            </Box>
           <Box sx={{

            display:"flex",
            // justifyContent:"space-between",
            flexDirection:"column",
            padding:"10px"
           }}>

           <Box  sx={{
                  fontSize: "12px",
                  lineHeight: "0.5rem",
                  fontWeight: "bold",
                  color: "#1c1c1c",
                  textTransform:"uppercase"

                }}>
            
               OUTLET NAME: {store.outletName}
              </Box>

              <Box sx={{
                  // fontSize: "12px",
                  fontSize: "12px",
                  lineHeight: "0.5rem",
                  fontWeight: "bold",
                  color: "#1c1c1c",
                  marginTop:"10px"
                  
                }}>
                 
          EMAIL: {store.email}
             
              </Box>
           </Box> */}
             
              
            
            {/* <Grid item  >
            <img
                        src={CompanyLogo}
                        alt="companyLogo"
                        className={classes.companyLogo}
                      />
              
            </Grid>
            
            
            <Grid item sm={12} md={6}>
              <strong>OUTLET NAME:</strong>{store.outletName}
              <Button>Edit</Button>
            </Grid> */}


          {/* </Grid>
          </Card>)} */}
         
          {/* <WrapperCard>
            <Box className={classes.flex}>
              <Box  sx={{
                  // fontSize: "12px",
                  lineHeight: "1rem",
                  fontWeight: "bold",
                  color: "#1c1c1c",
                }}>
            
                Baeonn@gmail.com
              </Box>
              <Box sx={{
                  // fontSize: "12px",
                   padding:"10px"
                  
                }}>
              <Chip label="Accepted"  color="primary"/>
             
              </Box>
            </Box>
            <Grid container>
                <Grid item xs={12} md={6}>
                Branch name:
                </Grid>
                <Grid item xs={12} md={6}>
                BAEONN fee:
                </Grid>
                <Grid item xs={12} md={6}>
                Sharing Rate:
                </Grid>
                <Grid item xs={12} md={6}>
                Conversion Rate:
                </Grid>
              </Grid>
              <Box sx={{textAlign:"end"}}>
                Edit
              <Switch
              color="primary"
  checked={true}
  // onChange={}
  inputProps={{ 'aria-label': 'controlled' }}
/>
</Box>
          </WrapperCard> */}
      
          
          
       
     
    </MainLayout>
  );
}

export default Store;
