import * as React from "react";
import PropTypes from "prop-types";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { DataGrid } from "@mui/x-data-grid";
import TableSelect from "../../components/table/selectTable";
import {
  makeStyles,
  Card,
  Grid,
  Button,
  TextField,
  Select,
  MenuItem,
  FormLabel,
  TextareaAutosize,
  Input,
  FormControlLabel,
  Radio,
  RadioGroup,
  createMuiTheme,
  
} from "@material-ui/core";
import PhotoCamera from "@mui/icons-material/PhotoCamera";
import BackupIcon from "@mui/icons-material/Backup";
import HeightIcon from "@mui/icons-material/Height";
import MainLayout from "../../layout/mainLayout";
import DatePicker from "@mui/lab/DatePicker";
import moment from 'moment'


import Stack from "@mui/material/Stack";
import { useSelector, useDispatch } from "react-redux";
import { addCampaignReducer } from "../../service config/configStore";
import FileUploadHoc from "../../components/File.Upload";
import { ToastContainer, toast } from "react-toastify";
import companyLogo from '../../assets/ill_header.png';
import {useNavigate} from "react-router-dom"

import {
  getMediaPartner,getStoreDetail,createCampaign
} from "../../service config/admin.service";

import Lable from "../../components/Typography/lable"


const useStyles = makeStyles((theme) => ({
  lable: {
    fontSize: "0.75rem",
    lineHeight: "1rem",
    fontWeight: "bold",
    color: "#57748a",
  },
  card: {
    maxWidth: "1000px",
    boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
    minHeight: "100vh",
    margin: "0px auto",
    marginTop: "100px",
    borderRadius: "5px",
    padding: "20px",
  },
  cancel: {
    background: "blue",
    color: "white",
    fontWeight: "700",
    "&:hover": {
      background: "blue",
      color: "white",
    },
  },
  inputfile: {
    display: "none",
  },
  upload: {
    background: "#f0f3f5",
    display: "flex",
    flexDirection: "column",
    width: "100%",
    height: "100%",
    justifyContent: "center",
  },
  icon: {
    transform: "rotate(180deg)",
  },
  grid: {
    padding: "25px 0",
  },
  input: {
    border: "none",
    boxShadow: "0 3px 5px rgb(0 0 0 / 0.1)",
    background: "white",
    "&:hover": {
      border: "none",
      outline: "none",
    },
  },
}));

function CreateCampaign(props) {

  const [rows,setRows]=React.useState([]);
  const classes = useStyles();
  const dispatch = useDispatch();
  const campaign  = useSelector((state) => state.campaignAdd.addCampaign);
  const mediaPartner= useSelector((state) => state.campaignAdd.mediaPartner);
  const advertiserId= useSelector((state) => state.storeDetail.storeDetail.id);
  let navigate = useNavigate();

  const submitDetails = async (e) => {
    e.preventDefault();
    const response = await createCampaign(campaign)
    if (!response.data.error) {
      navigate(`/campaignPayment?id=${response.data.payload.id}`);
    }
    else{
      toast.error(response.data.payload);
    }
}
 
  const column = [
    {
      field: "id",
      headerName: "Id",
      headerAlign: "center",
      align: "center",
      width: 100,
      hidden: true,
    },
    {
      field: "created",
      headerName: "created",
      width: 150,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "title",
      headerName: "Title",
      width: 150,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "description",
      headerName: "Description",
      width: 400,
      align: "left",
      headerAlign: "left",
    },
    {
      field: "numberOfCoupons",
      headerName: "No Of Coupons",
      type: "number",
      width: 120,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "shared",
      headerName: "Shared",
      type: "number",
      width: 120,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "notshared",
      headerName: "Not shared",
      type: "number",
      width: 120,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "status",
      headerName: "status",
      width: 150,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "action",
      headerName: "Action",
      width: 150,
      align: "center",
      headerAlign: "center",
      renderCell: (params) => {
        return (
         
          <Button
            color="success"
            size="small"
            // onClick={() => onSelect()}
            className={classes.actionBtnpay}
          >
            Share
          </Button>
        );
      },
    },
  ];

 
  
  
  

 
  
 React.useEffect( () =>{
  getMediaPartner();
  getStoreDetail();
    dispatch(
      addCampaignReducer({startCampaign :moment().toDate(), 
      endCampaign: moment().add(7, 'days').toDate(),
      couponEndDate: moment().add(14, 'days').toDate()})
    )},[])

    React.useEffect(() => {
      let row = []
      for(let i = 0; i < mediaPartner.length; i++){
          let obj =   {
            id: mediaPartner[i].id,
            domain: mediaPartner[i].category,
            distance: 32,
            mediapartner: mediaPartner[i].outletName,
            converstionrate: 12.3,
            sharingrate: 12.3,
            fee: mediaPartner[i].mediaPartnerFee,
            logo: companyLogo,
          }
          row.push(obj);
      }
      setRows(row);
      
      
  }, [mediaPartner]);

  React.useEffect(()=>{
    dispatch(
      addCampaignReducer({advertiserId:advertiserId, 
      })
    )
  },[advertiserId])

    const setimageUrl = (data, fileType) => {
          dispatch(addCampaignReducer({ couponImage: data.data.payload.Location }));
      }
    

  return (
    <MainLayout>
      <form onSubmit={submitDetails}>
      <Card variant="outlined" className={classes.card}>
        
        <Grid
          container
          columnSpacing={{ xs: 1, sm: 2, md: 3 }}
          className={classes.grid}
        >
          <Grid item>
            <Typography
              variant="h6"
              display="block"
              sx={{
                // fontSize: "12px",
                lineHeight: "1rem",
                fontWeight: "bold",
                color: "#1c1c1c",
              }}
              className={classes.lable}
            >
              Add New Coupon
            </Typography>
          </Grid>
        </Grid>{" "}
        <Stack
          spacing={2}
          sx={{ p: 2 }}
          
        >
          <div>
            <div>
              <Grid container spacing={2} columns={{ xs: 4, md: 12 }}>
                <Grid item xs={12} md={6}>
                  <div>
                    
                    <Lable style={{my: 2 ,marginRight: "auto",}} title=" COUPON TITLE"/>

                    <TextField
                      id="outlined-basic"
                      variant="outlined"
                      fullWidth
                      sx={{
                        border: "none",
                      }}
                      value={campaign.title}
                      onChange={(e) =>
                        dispatch(
                          addCampaignReducer({
                            title: e.target.value,
                          })
                        )
                      }
                    />
                  </div>
                </Grid>

                <Grid item xs={12} md={6}>
                  <div>
                 
                    <Lable style={{my: 2 ,marginRight: "auto",}} title=" COUPON DISCRIPTION"/>

                    <TextField
                      id="outlined-basic"
                      variant="outlined"
                      fullWidth
                      sx={{
                        border: "none",
                      }}
                      value={campaign.description}
                      onChange={(e) =>
                        dispatch(
                          addCampaignReducer({
                            description: e.target.value,
                          })
                        )
                      }
                    />
                  </div>
                </Grid>
              </Grid>
            </div>
          </div>

          <div>
            <Grid container spacing={2} columns={{ xs: 4, md: 12 }}>
              <Grid item  xs={12} md={4}>
                <div>
                
                  <Lable style={{my: 2 ,marginRight: "auto",}} title="NO. OF COUPONS"/>

                  <TextField
                    id="outlined-basic"
                    variant="outlined"
                    fullWidth
                    type="number"
                    sx={{
                      border: "none",
                    }}
                    value={campaign.noOfCoupons}
                    onChange={(e) =>
                      dispatch(
                        addCampaignReducer({
                          noOfCoupons: e.target.value,
                        })
                      )
                    }
                  />
                </div>
              </Grid>
              <Grid item xs={12} md={4}>
                <div>
                 
                  <Lable style={{my: 2 ,marginRight: "auto",}} title=" LINK TO APP"/>

                  <TextField
                    id="outlined-basic"
                    variant="outlined"
                    fullWidth
                    sx={{
                      border: "none",
                    }}
                    value={campaign.linkToApp}
                    onChange={(e) =>
                      dispatch(
                        addCampaignReducer({
                          linkToApp:  e.target.value,
                        })
                      )
                    }
                  />
                </div>
              </Grid>
              <Grid item xs={12} md={4}>
                <div>
                 
                  <Lable style={{my: 2 ,marginRight: "auto",}} title="LINK TO WEBSITE"/>

                  <TextField
                    id="outlined-basic"
                    variant="outlined"
                    fullWidth
                    sx={{
                      border: "none",
                    }}
                    value={campaign.linkToWebsite }
                    onChange={(e) =>
                      dispatch(
                        addCampaignReducer({
                          linkToWebsite :  e.target.value,
                        })
                      )
                    }
                  />
                </div>
              </Grid>
            </Grid>
          </div>

          <div>
            <Divider />
          </div>
          <div>
            <div>
              <Grid container spacing={2} columns={{ xs: 4, md: 12 }}>
                <Grid item xs={12} md={6}>
                  <div>
                    
                    <Lable style={{my: 2 ,marginRight: "auto",}} title="  START DATE"/>

                    <DatePicker
                      value={campaign.startCampaign}
                      onChange={(newValue) => {
                        dispatch(
                          addCampaignReducer({
                            startCampaign :  newValue,
                          })
                        )
                      }}
                     
                      minDate={moment().toDate()}
                            
                      renderInput={(params) => (
                        <TextField {...params} variant="outlined" fullWidth  />
                      )}
                    />
                  </div>
                </Grid>

                <Grid item xs={12} md={6}>
                  <div>
                   
                    <Lable style={{my: 2 ,marginRight: "auto",}} title=" END DATE"/>

                    <DatePicker
                      onChange={(newValue) => {
                        dispatch(
                          addCampaignReducer({
                            endCampaign :  newValue,
                          })
                        )
                      }}
                      mask="YYYY-MM-DD"
                      minDate={moment(campaign.startCampaign).add(3, 'days').toDate()}
                      value={campaign.endCampaign }
                      renderInput={(params) => (
                        <TextField {...params} variant="outlined" fullWidth />
                      )}
                    />
                  </div>
                </Grid>
              </Grid>
            </div>
          </div>
          <div>
            <div>
              <Grid container spacing={2} columns={{ xs: 4, md: 12 }}>
                <Grid item xs={6}>
                  <div>
                    
                    <Lable style={{my: 2 ,marginRight: "auto",}} title=" EXPIRY DATE OF REWARD CODE"/>

                    <DatePicker
                     onChange={(newValue) => {
                      dispatch(
                        addCampaignReducer({
                          couponEndDate:  newValue,
                        })
                      )
                    }}
                    minDate={moment(campaign.endCampaign).add(3, 'days').toDate()}
                    value={campaign.couponEndDate }
                      renderInput={(params) => (
                        <TextField {...params} variant="outlined" fullWidth />
                      )}
                    />
                  </div>
                </Grid>
              </Grid>
            </div>
          </div>

          <div>
            <div>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                <FileUploadHoc fileType="couponImage" set={setimageUrl}>
                  <Box
                    component="label"
                    sx={{
                      width: "90%",
                      margin: "0 auto",
                      padding: "10px",
                      background: "white",
                      height: "100px",
                      display: "block",
                      boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
                    }}
                    htmlFor="couponImage"
                  >
                    <Input
                      accept="image/*"
                      id="couponImage"
                      type="file"
                      className={classes.inputfile}
                    />
                    <IconButton
                      color="primary"
                      aria-label="upload picture"
                      component="span"
                      className={classes.upload}
                    >
                      <BackupIcon
                        sx={{
                          fontSize: "40px",
                        }}
                      />
                     
                      <Lable style={{my: 2 ,textAlign:"center"}} title="Upload campaigns Image"/>

                    </IconButton>
                  </Box>
                  </FileUploadHoc>
                  {campaign.couponImage ? "Last uploaded:" : ""}{" "}
                      <span>
                        {campaign.couponImage ? (
                          <a href={campaign.couponImage} target="_blank">
                            Click Here
                          </a>
                        ) : (
                          ""
                        )}
                      </span>
                </Grid>
              </Grid>
            </div>
          </div>
        </Stack>
        <Stack mt={4} p={5}>
            <TableSelect rows={rows} />
        </Stack>
        <Box sx={{ textAlign: "center" }}>
        <Button
                  color="success"
                  size="small"
                  type="submit"
                  // onClick={(e) => handleSubmit(e, "add")}
                  className={classes.cancel}
                >
                  Submit
                </Button>
                </Box >
      </Card>
      </form>
    </MainLayout>
  );
}

export default CreateCampaign;
