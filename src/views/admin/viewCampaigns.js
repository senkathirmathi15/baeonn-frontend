import React from 'react';
import MainLayout from "../../layout/mainLayout";
import {
  makeStyles,
  Grid,
  Box,
  IconButton,
  TextField,
  Select,
  MenuItem,
  FormLabel,
  TextareaAutosize,
  Input,
  FormControlLabel,
  Radio,
  RadioGroup,
  createMuiTheme,
} from "@material-ui/core";
import CardShareCoupons from '../../components/Cards/CardCampaigns';
import { useSelector, useDispatch } from "react-redux";
import {
  getUserStore,
  getStoreCampaign
} from "../../service config/admin.service";
import moment from 'moment';


const useStyles = makeStyles((theme) => ({
  cards: {
    marginTop: "100px",
  },
  
}));

export default function ViewCampaigns() {
          const classes = useStyles();
          const viewCampaign = useSelector((state) => state.campaignDetails.viewCampaign.viewDetails);
          const id = useSelector((state) => state.campaignDetails.viewCampaign.storeId);
          const [rows, setRows] = React.useState([]);
          React.useEffect(()=>{
            getUserStore();
          },[])
          React.useEffect(()=>{
            makeRows(viewCampaign)
          },[viewCampaign])

          function makeRows(cam) {
            const row = [];
            for (let i = 0; i < cam.length; i ++) {
              row.push({
                sno: i,
                id: cam[i].id,
                created: moment(cam[i].created).format("DD/MM/YYYY"),
                title: cam[i].title,
                description: cam[i].description,
                numberOfCoupons: cam[i].noOfCoupons,
                shared: cam[i].noOfCouponsShared,
                notshared: cam[i].noOfCoupons - cam[i].noOfCouponsShared,
                paymentstatus: cam[i].paymentStatus,
                action: () => {
                  console.log('KFC');
                },
              })
            }
            // console.log('row only', row)
            setRows(row);
          }

  return (
    <>
    <MainLayout>
        <div className={classes.cards}>
            <CardShareCoupons rows={rows}/>  
        </div>
    </MainLayout>
    </>
  )
}
