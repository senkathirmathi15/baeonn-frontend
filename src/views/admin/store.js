import * as React from "react";
import PropTypes from "prop-types";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import TableSelect from "../../components/table/selectTable";
import { useSelector, useDispatch } from "react-redux";
import { storeReducer } from "../../service config/configStore";
import FileUploadHoc from "../../components/File.Upload";
import { ToastContainer, toast } from "react-toastify";
import Chip from "@mui/material/Chip";
import MuiPhoneNumber from "material-ui-phone-number";
import Lable from "../../components/Typography/lable"

import {
  submitStoreDetail,
  updateStoreSubmit,
  getStoreDetail,
} from "../../service config/admin.service";

import {
  makeStyles,
  Card,
  Grid,
  Button,
  TextField,
  Select,
  MenuItem,
  FormLabel,
  TextareaAutosize,
  Input,
  FormControlLabel,
  Radio,
  RadioGroup,
  Switch,
  createMuiTheme,
   
} from "@material-ui/core";
import PhotoCamera from "@mui/icons-material/PhotoCamera";
import BackupIcon from "@mui/icons-material/Backup";
import HeightIcon from "@mui/icons-material/Height";
import MainLayout from "../../layout/mainLayout";
import WrapperCard from "../../components/wrapperCard/wrapperCard";

import Stack from "@mui/material/Stack";

const drawerWidth = 270;
const theme = createMuiTheme({
  overrides: {
    MuiInput: {
      underline: {
        "&:hover:not($disabled):before": {
          backgroundColor: "rgba(0, 188, 212, 0.7)",
        },
      },
    },
  },
});
function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData("Frozen yoghurt", 159, 6.0, 24, 4.0),
  createData("Ice cream sandwich", 237, 9.0, 37, 4.3),
  createData("Eclair", 262, 16.0, 24, 6.0),
  createData("Cupcake", 305, 3.7, 67, 4.3),
  createData("Gingerbread", 356, 16.0, 49, 3.9),
];

const useStyles = makeStyles((theme) => ({
  lable: {
    fontSize: "0.75rem",
    lineHeight: "1rem",
    fontWeight: "bold",
    color: "#57748a",
  },
  card: {
    maxWidth: "1000px",
    boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
    // minHeight: "100vh",
    margin: "0px auto",
    marginTop: "100px",
    borderRadius: "5px",
    padding: "20px",
  },
  cancel: {
    background: "blue",
    color: "white",
    fontWeight: "700",
    "&:hover": {
      background: "blue",
      color: "white",
    },
  },
  inputfile: {
    display: "none",
  },
  upload: {
    background: "#f0f3f5",
    display: "flex",
    flexDirection: "column",
    width: "100%",
    height: "100%",
    justifyContent: "center",
  },
  icon: {
    transform: "rotate(180deg)",
  },
  grid: {
    padding: "25px 0",
  },
  input: {
    border: "none",
    boxShadow: "0 3px 5px rgb(0 0 0 / 0.1)",
    background: "white",
    "&:hover": {
      border: "none",
      outline: "none",
    },
  },
  radio: {
    "&$checked": {
      color: "#4B8DF8",
    },
  },
  flex:{
    display:"flex",
    justifyContent:"space-between",
    flex: "wrap",
    [theme.breakpoints.down('sm')]: {
      flexDirection:"column"
    },
  },
  checked: {},
  edit: {},
}));

const states = ["Tamil Nadu"];
const cities = [
  "Chennai",
  "Coimbatore",
  "Madurai",
  "Tiruchirappalli",
  "Salem",
  "Tirunelveli",
  "Tiruppur",
  "Vellore",
  "Erode",
  "Thoothukkudi",
  "Dindigul",
  "Thanjavur",
  "Ranipet",
  "Sivakasi",
  "Karur",
  "Udhagamandalam",
  "Hosur",
  "Nagercoil",
  "Kanchipuram",
  "Kumarapalayam",
  "Karaikkudi",
  "Neyveli",
  "Cuddalore",
  "Kumbakonam",
  "Tiruvannamalai",
  "Pollachi",
  "Rajapalayam",
  "Gudiyatham",
  "Pudukkottai",
  "Vaniyambadi",
  "Ambur",
  "Nagapattinam",
];
const categories = [
  "Supermarket/Hypermarket/Grocery Store",
  "Restaurant/Food & Beverage/Canteen",
  "Bakery/Ice cream parlor/Cafe ",
  "Pharmacies/Wellness Stores",
  "Fitness centre/yoga centre",
  "Mobile stores",
  "Beauty parlour/Saloon/Spa",
  "Footwear stores",
  "Textile/Boutique/Tailor shop",
  "Fancy Stores ",
  "Jewelry shop",
  "Opticals",
  "Hotels/Lodges ",
  "Cinema Theatre",
  "Furnitures/Home Appliances",
  "Auto spares and electricals/Hardwares",
  "Fireworks/Crackers shop",
];

function Store(props) {
  const storeDetail = useSelector((state) => state.storeDetail.storeDetail);
  const id = useSelector((state) => state.storeDetail.storeDetail.id);

  const initialState = { day: "Monday", from: "00:00", to: "00:00" };
  const [isUpdate, setIsUpdate] = React.useState(false); // if store then don't show add store on load else show;
  const [isEdit, setIsEdit] = React.useState(false); // if store then don't show add store on load else show;

  const [newDateTimeSlot, setNewDateTimeSlot] = React.useState(initialState);

  React.useEffect(() => {
    getStoreDetail();
  }, []);

  const checkIfSlotAlreadyIn = (day) => {
    for (let slot of storeDetail.daysOpen) {
      if (slot.day === day) return true;
    }
    return false;
  };

  const addDayTime = (dayTimeSlots) =>
    dispatch(
      storeReducer({ daysOpen: [...storeDetail.daysOpen, dayTimeSlots] })
    );
  const removeDayTime = (dayTimeSlots) =>
    dispatch(storeReducer({ daysOpen: dayTimeSlots }));

  const removeDayTimeSlot = (e, { day }) => {
    e.preventDefault();
    let newSlot = storeDetail.daysOpen.filter((slot) => day !== slot.day);
    console.log(newSlot);
    // console.log({newSlot})
    removeDayTime(newSlot);
  };
  const set = (e) => {
    e.preventDefault();
    console.log(newDateTimeSlot.day, newDateTimeSlot.from, newDateTimeSlot.to);
    if (!newDateTimeSlot.day || !newDateTimeSlot.from || !newDateTimeSlot.to)
      return;
    if (!checkIfSlotAlreadyIn(newDateTimeSlot.day)) {
      addDayTime(newDateTimeSlot);
      setNewDateTimeSlot(initialState);
    }
    console.log("slot already present.");
  };
  const dispatch = useDispatch();
  const setimageUrl = (data, fileType) => {
    switch (fileType) {
      case "storelogo":
        dispatch(storeReducer({ storeLogoUrl: data.data.payload.Location }));
        break;
      case "storeimage":
        dispatch(storeReducer({ storeImageUrl: data.data.payload.Location }));
        break;
      default:
        break;
    }
  };
  const handleSubmit = async (event, type) => {
    event.preventDefault();
    switch (type) {
      case "add":
        const res = await submitStoreDetail(storeDetail);
        if (!res.error) {
          toast.success(res.payload);
        } else {
          toast.error(res.payload);
        }

        break;
      case "update":
        const response = await updateStoreSubmit({
          id: id,
          store: storeDetail,
        });
        if (!response.error) {
          toast.success(response.payload);
        } else {
          toast.error(response.payload);
        }

        break;

      default:
        break;
    }
   
  };

  const rows = [
    createData("Frozen yoghurt", "Monday", "10.30AM"),
    createData("Ice cream sandwich", "Monday", "10.30AM"),
  ];
  const classes = useStyles();
  return (
    <MainLayout>
      <Card variant="outlined" className={classes.card}>
        {id !== "" && !isEdit ? (
          <>
          <Grid
            container
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
            className={classes.grid}
          >
            <Grid item xs={9}>
              <Typography
                variant="h6"
                display="block"
                sx={{
                  // fontSize: "12px",
                  lineHeight: "1rem",
                  fontWeight: "bold",
                  color: "#1c1c1c",
                }}
                className={classes.lable}
              >
                Store Details
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Box
                sx={{
                  textAlign: "end",
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "end",
                }}
              >
                <Chip
                  label={storeDetail.outletName}
                  variant="outlined"
                  sx={{
                    background: "green",
                    color: "white",
                    padding: "10px",
                    "&:hover": {
                      background: "green",
                      color: "white",
                    },
                  }}
                />
                <Chip
                  label="100"
                  variant="outlined"
                  className={classes.edit}
                  sx={{
                    background: "green",
                    color: "white",
                    padding: "10px",
                    "&:hover": {
                      background: "green",
                      color: "white",
                    },
                  }}
                />
                <Chip
                  label="edit"
                  sx={{
                    background: "green",
                    color: "white",
                    padding: "10px",
                    "&:hover": {
                      background: "green !important",
                      color: "white",
                    },
                  }}
                  variant="outlined"
                  onClick={() => setIsEdit(!isEdit)}
                />
              </Box>
            </Grid>
          </Grid>
         <Stack spacing={2}>
          <WrapperCard>
            <Box className={classes.flex}>
              <Box  sx={{
                  // fontSize: "12px",
                  lineHeight: "1rem",
                  fontWeight: "bold",
                  color: "#1c1c1c",

                }}>
            
                Baeonn@gmail.com
              </Box>
              <Box sx={{
                  // fontSize: "12px",
                   padding:"10px"
                  
                }}>
              <Chip label="pending"  color="warning"/>
              </Box>
              

            </Box>
            <Box sx={{textAlign:"end"}}>
            <Button variant="contained" size="small" className={classes.cancel}>Resend</Button>

              </Box>
          </WrapperCard>
          <WrapperCard>
            <Box className={classes.flex}>
              <Box  sx={{
                  // fontSize: "12px",
                  lineHeight: "1rem",
                  fontWeight: "bold",
                  color: "#1c1c1c",
                }}>
            
                Baeonn@gmail.com
              </Box>
              <Box sx={{
                  // fontSize: "12px",
                   padding:"10px"
                  
                }}>
              <Chip label="Accepted"  color="primary"/>
             
              </Box>
            </Box>
            <Grid container>
                <Grid item xs={12} md={6}>
                Branch name:
                </Grid>
                <Grid item xs={12} md={6}>
                BAEONN fee:
                </Grid>
                <Grid item xs={12} md={6}>
                Sharing Rate:
                </Grid>
                <Grid item xs={12} md={6}>
                Conversion Rate:
                </Grid>
              </Grid>
              <Box sx={{textAlign:"end"}}>
                Edit
              <Switch
              color="primary"
  checked={true}
  // onChange={}
  inputProps={{ 'aria-label': 'controlled' }}
/>
</Box>
          </WrapperCard>
          </Stack>
          
          </>
        ) : (
          <form>
            <Grid
              container
              columnSpacing={{ xs: 1, sm: 2, md: 3 }}
              className={classes.grid}
            >
              <Grid item xs={12} md={6}>
                <Typography
                  variant="h6"
                  display="block"
                  sx={{
                    // fontSize: "12px",
                    lineHeight: "1rem",
                    fontWeight: "bold",
                    color: "#1c1c1c",
                  }}
                  className={classes.lable}
                >
                  {id !== "" ? "Edit store details" : "Add store details"}
                </Typography>
              </Grid>
              <Grid item xs={12} md={6}>
                <Box sx={{ textAlign: "end" }}>
                  {id !== "" ? (
                    <Button
                      color="success"
                      size="small"
                      onClick={() => setIsEdit(!isEdit)}
                      className={classes.cancel}
                    >
                      Cancel
                    </Button>
                  ) : null}
                </Box>
              </Grid>
            </Grid>
            <Stack spacing={2} sx={{ py:2}}>
              <div>
               
              <Lable style={{my: 2 ,marginRight: "auto",}} title="OUTLET NAME"/>

                <TextField
                  id="outlined-basic"
                  variant="outlined"
                  fullWidth
                  value={storeDetail.outletName}
                  className={classes.input}
                  InputProps={{ classes: { underline: classes.input } }}
                  sx={{
                    border: "none",
                  }}
                  onChange={(e) =>
                    dispatch(
                      storeReducer({
                        outletName: e.target.value,
                      })
                    )
                  }
                />
              </div>
              <Grid container >
              <Grid item xs={12} >
              
                
                <Lable style={{my: 2 ,marginRight: "auto",}} title="SELECT A CATEGORY"/>
                <Select
                
                  value={storeDetail.category}
                  // onChange={handleChange}
                  displayEmpty
                  inputProps={{ "aria-label": "Without label" }}
                  fullWidth
                  variant="outlined"
                  onChange={(e) =>
                    dispatch(
                      storeReducer({
                        category: e.target.value,
                      })
                    )
                  }
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  {categories.map((categorie) => (
                    <MenuItem value={categorie}>{categorie}</MenuItem>
                  ))}
                </Select>{" "}
              
              </Grid>
              </Grid>

              <div>
                <Grid container spacing={2}>
                  <Grid item xs={12} md={4}>
                    <div>
                      
                      <Lable style={{my: 2 ,marginRight: "auto",}} title=" NUMBER OF BILLS PER DAY"/>
                      <TextField
                        value={storeDetail.noOfWalkins}
                        id="outlined-basic"
                        variant="outlined"
                        fullWidth
                        type="number"
                        sx={{
                          border: "none",
                        }}
                        onChange={(e) =>
                          dispatch(
                            storeReducer({
                              noOfWalkins: e.target.value,
                            })
                          )
                        }
                      />
                    </div>
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <div>
                     
                      <Lable style={{my: 2 ,marginRight: "auto",}} title="AVERAGE BILL VALUE"/>
                      <TextField
                        value={storeDetail.avgBillValue}
                        id="outlined-basic"
                        variant="outlined"
                        fullWidth
                        type="number"
                        sx={{
                          border: "none",
                        }}
                        onChange={(e) =>
                          dispatch(
                            storeReducer({
                              avgBillValue: e.target.value,
                            })
                          )
                        }
                      />
                    </div>
                  </Grid>
                </Grid>
              </div>
              <div>
              
                <Lable style={{my: 2 ,marginRight: "auto",}} title="OUTLET ADDRESS"/>
                <Divider />
              </div>
              <div>
                <Grid container spacing={2} columns={{ xs: 4, md: 12 }}>
                  <Grid item xs={12} md={4}>
                    <div>
                     
                      <Lable style={{my: 2 ,marginRight: "auto",}} title="PIN CODE"/>
                      <TextField
                        value={storeDetail.pincode}
                        id="outlined-basic"
                        variant="outlined"
                        fullWidth
                        type="number"
                        sx={{
                          border: "none",
                        }}
                        onChange={(e) =>
                          dispatch(
                            storeReducer({
                              pincode: e.target.value,
                            })
                          )
                        }
                      />
                    </div>
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <div>
                     
                      <Lable style={{my: 2 ,marginRight: "auto",}} title="STATE"/>
                      <Select
                        value={storeDetail.state}
                        // onChange={handleChange}
                        displayEmpty
                        inputProps={{ "aria-label": "Without label" }}
                        fullWidth
                        variant="outlined"
                        onChange={(e) =>
                          dispatch(
                            storeReducer({
                              state: e.target.value,
                            })
                          )
                        }
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        {states.map((state) => (
                          <MenuItem value={state}>{state}</MenuItem>
                        ))}
                      </Select>{" "}
                    </div>
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <div>
                      
                      <Lable style={{my: 2 ,marginRight: "auto",}} title="CITY"/>
                      <Select
                        value={storeDetail.city}
                        // onChange={handleChange}
                        displayEmpty
                        inputProps={{ "aria-label": "Without label" }}
                        fullWidth
                        variant="outlined"
                        onChange={(e) =>
                          dispatch(
                            storeReducer({
                              city: e.target.value,
                            })
                          )
                        }
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        {cities.map((city) => (
                          <MenuItem value={city}>{city}</MenuItem>
                        ))}
                      </Select>{" "}
                    </div>
                  </Grid>
                </Grid>
              </div>
              <div>
                <Grid container>
                  <Grid item xs={12} md={6}>
                    <div>
                      
                      <Lable style={{my: 2 ,marginRight: "auto",}} title="ADDRESS"/>
                      <TextField
                        value={storeDetail.address}
                        id="outlined-basic"
                        variant="outlined"
                        fullWidth
                        multiline
                        type="text"
                        sx={{
                          border: "none",
                        }}
                        onChange={(e) =>
                          dispatch(
                            storeReducer({
                              address: e.target.value,
                            })
                          )
                        }
                      />
                    </div>
                  </Grid>
                </Grid>
              </div>
              <div>
                <Divider />
              </div>
              <div>
                <div>
                  <Grid container spacing={2} columns={{ xs: 4, md: 12 }}>
                    <Grid item xs={12} md={6}>
                      <div>
                        
                        <Lable style={{my: 2 ,marginRight: "auto",}} title="STORE EMAIL"/>
                        <TextField
                          value={storeDetail.email}
                          id="outlined-basic"
                          variant="outlined"
                          fullWidth
                          type="email"
                          sx={{
                            border: "none",
                          }}
                          onChange={(e) =>
                            dispatch(
                              storeReducer({
                                email: e.target.value,
                              })
                            )
                          }
                        />
                      </div>
                    </Grid>

                    <Grid item xs={12} md={6}>
                      <div>
                       
                        <Lable style={{my: 2 ,marginRight: "auto",}} title="CUSTOMER CARE CONTACT"/>

                        <MuiPhoneNumber
            name="phone"
            id="phone"
            placeholder="Enter"
            // data-cy="user-phone"
            defaultCountry={"in"}
            fullWidth
            variant="outlined"
            value={storeDetail.customerCareContact}
            sx={{
              border: "none",
            }}
           
           
            onChange={(value) =>
              dispatch(
                storeReducer({
                  customerCareContact: value,
                })
              )
            }
          />

                       
                      </div>
                    </Grid>
                  </Grid>
                </div>
              </div>
              <div>
                <div>
                  <Grid container spacing={2}>
                    <Grid item xs={12} md={6}>
                      <FileUploadHoc fileType="storeimage" set={setimageUrl}>
                        <Box
                          component="label"
                          sx={{
                            width: "90%",
                            margin: "0 auto",
                            padding: "10px",
                            background: "white",
                            height: "100px",
                            display: "block",
                            boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
                          }}
                          htmlFor="storeimage"
                        >
                          <Input
                            accept="image/*"
                            id="storeimage"
                            type="file"
                            className={classes.inputfile}
                          />
                          <IconButton
                            color="primary"
                            aria-label="upload picture"
                            component="span"
                            className={classes.upload}
                          >
                            <BackupIcon
                              sx={{
                                fontSize: "40px",
                              }}
                            />
                           
                            <Lable style={{my: 2 ,textAlign:"center"}} title="Upload Store Image"/>
                          </IconButton>
                        </Box>
                      </FileUploadHoc>
                      {storeDetail.storeImageUrl ? "Last uploaded:" : ""}{" "}
                      <span>
                        {storeDetail.storeImageUrl ? (
                          <a href={storeDetail.storeImageUrl} target="_blank">
                            Click Here
                          </a>
                        ) : (
                          ""
                        )}
                      </span>
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <FileUploadHoc fileType="storelogo" set={setimageUrl}>
                        <Box
                          component="label"
                          sx={{
                            width: "90%",
                            margin: "0 auto",
                            padding: "10px",
                            background: "white",
                            height: "100px",
                            display: "block",
                            boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
                          }}
                          htmlFor="storelogo"
                        >
                          <Input
                            accept="image/*"
                            id="storelogo"
                            type="file"
                            className={classes.inputfile}
                          />
                          <IconButton
                            color="primary"
                            aria-label="upload picture"
                            component="span"
                            className={classes.upload}
                          >
                            <BackupIcon
                              sx={{
                                fontSize: "40px",
                              }}
                            />
                           
                            <Lable style={{my: 2 ,textAlign:"center"}} title="Upload Logo"/>
                          </IconButton>
                        </Box>
                      </FileUploadHoc>
                      {storeDetail.storeLogoUrl ? "Last uploaded:" : ""}{" "}
                      <span>
                        {storeDetail.storeLogoUrl ? (
                          <a href={storeDetail.storeLogoUrl} target="_blank">
                            Click Here
                          </a>
                        ) : (
                          ""
                        )}
                      </span>
                    </Grid>
                  </Grid>
                </div>
              </div>
              <div>
                
                <Lable style={{my: 2 ,marginRight: "auto",}} title=" HOURS OF OPERATION"/>
                <Divider />
              </div>
              <div>
                <RadioGroup
                  row
                  aria-labelledby="demo-radio-buttons-group-label"
                  defaultValue="Monday"
                  name="radio-buttons-group"
                  onClick={(e) =>
                    setNewDateTimeSlot({
                      ...newDateTimeSlot,
                      day: e.target.value,
                    })
                  }
                >
                  <FormControlLabel
                    value="Monday"
                    control={
                      <Radio
                        classes={{
                          root: classes.radio,
                          checked: classes.checked,
                        }}
                      />
                    }
                    label="Monday"
                  />
                  <FormControlLabel
                    value="Tuseday"
                    control={
                      <Radio
                        classes={{
                          root: classes.radio,
                          checked: classes.checked,
                        }}
                      />
                    }
                    label="Tuseday"
                  />
                  <FormControlLabel
                    value="Wednesday"
                    control={
                      <Radio
                        classes={{
                          root: classes.radio,
                          checked: classes.checked,
                        }}
                      />
                    }
                    label="Wednesday"
                  />
                  <FormControlLabel
                    value="Thrusday"
                    control={
                      <Radio
                        classes={{
                          root: classes.radio,
                          checked: classes.checked,
                        }}
                      />
                    }
                    label="Thrusday"
                  />
                  <FormControlLabel
                    value="Friday"
                    control={
                      <Radio
                        classes={{
                          root: classes.radio,
                          checked: classes.checked,
                        }}
                      />
                    }
                    label="Friday"
                  />
                  <FormControlLabel
                    value="Saturday"
                    control={
                      <Radio
                        classes={{
                          root: classes.radio,
                          checked: classes.checked,
                        }}
                      />
                    }
                    label="Saturday"
                  />
                  <FormControlLabel
                    value="other"
                    control={
                      <Radio
                        classes={{
                          root: classes.radio,
                          checked: classes.checked,
                        }}
                      />
                    }
                    label="Sunday"
                  />
                </RadioGroup>
              </div>
              <div>
                <Grid container spacing={2} columns={{ xs: 4, md: 12 }}>
                  <Grid item xs={12} md={4}>
                    <div>
                      
                      <Lable style={{my: 2 ,marginRight: "auto",}} title="FROM"/>
                      <Select
                        value={newDateTimeSlot.from}
                        displayEmpty
                        inputProps={{ "aria-label": "Without label" }}
                        fullWidth
                        variant="outlined"
                        onChange={(e) => {
                          setNewDateTimeSlot({
                            ...newDateTimeSlot,
                            from: e.target.value,
                          });
                        }}
                      >
                        <MenuItem value="00:00">SELECT TIME</MenuItem>
                        <MenuItem value="10:00">10:00</MenuItem>
                        <MenuItem value="11:00">11:00</MenuItem>
                        <MenuItem value="12:00">12:00</MenuItem>
                        <MenuItem value="13:00">13:00</MenuItem>
                      </Select>{" "}
                    </div>
                  </Grid>
                  <Grid item xs={12} md={4}></Grid>
                  <Grid item xs={12} md={4}>
                    <div>
                    
                      <Lable style={{my: 2 ,marginRight: "auto",}} title="TO"/>
                      <Select
                        value={newDateTimeSlot.to}
                        // onChange={handleChange}
                        displayEmpty
                        inputProps={{ "aria-label": "Without label" }}
                        fullWidth
                        variant="outlined"
                        onChange={(e) => {
                          setNewDateTimeSlot({
                            ...newDateTimeSlot,
                            to: e.target.value,
                          });
                        }}
                      >
                        <MenuItem value="00:00">
                          <em>SELECT TIME</em>
                        </MenuItem>
                        <MenuItem value="10:00">10:00</MenuItem>
                        <MenuItem value="11:00">11:00</MenuItem>
                        <MenuItem value="12:00">12:00</MenuItem>
                        <MenuItem value="13:00">13:00</MenuItem>
                      </Select>{" "}
                    </div>
                  </Grid>
                </Grid>
              </div>
              <div>
                <Button
                  color="success"
                  size="large"
                  onClick={set}
                  className={classes.cancel}
                >
                  Add slot
                </Button>
              </div>
              <Box
                sx={{ width: { xs: "300", sm: "500" }, overflowX: "scroll" }}
              >
                <TableContainer
                  component={Paper}
                  sx={{
                    textAlign: "center",
                    display: "flex",
                    justifyContent: "start",
                  }}
                >
                  <Table
                    sx={{ maxWidth: 700, textAlign: "start" }}
                    aria-label="simple table"
                  >
                    <TableHead>
                      <TableRow>
                        <TableCell
                          align="center"
                          sx={{
                            fontSize: "12px",
                            lineHeight: "1rem",
                            fontWeight: "bold",
                            color: "#1c1c1c",
                          }}
                        >
                          DAY
                        </TableCell>
                        <TableCell
                          align="center"
                          sx={{
                            fontSize: "12px",
                            lineHeight: "1rem",
                            fontWeight: "bold",
                            color: "#1c1c1c",
                          }}
                        >
                          TIME
                        </TableCell>
                        <TableCell
                          align="center"
                          sx={{
                            fontSize: "12px",
                            lineHeight: "1rem",
                            fontWeight: "bold",
                            color: "#1c1c1c",
                          }}
                        >
                          ACTION
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {storeDetail.daysOpen.length > 0 &&
                        storeDetail.daysOpen.map((slot) => (
                          <TableRow
                            key={slot.name}
                            sx={{
                              "&:last-child td, &:last-child th": {
                                border: "none",
                              },
                            }}
                          >
                            <TableCell align="center">{slot.day}</TableCell>
                            <TableCell align="center">
                              {slot.from}-{slot.to}
                            </TableCell>
                            <TableCell align="center">
                              <Button
                                color="success"
                                size="large"
                                className={classes.cancel}
                                onClick={(e) => removeDayTimeSlot(e, slot)}
                              >
                                Remove slot
                              </Button>
                            </TableCell>
                          </TableRow>
                        ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              </Box>
            </Stack>
            <Box sx={{ textAlign: "center" }}>
              {id !== "" ? (
                <Button
                  color="success"
                  size="small"
                  type="submit"
                  onClick={(e) => handleSubmit(e, "update")}
                  className={classes.cancel}
                >
                  update
                </Button>
              ) : (
                <Button
                  color="success"
                  size="small"
                  type="submit"
                  onClick={(e) => handleSubmit(e, "add")}
                  className={classes.cancel}
                >
                  ADD
                </Button>
              )}
            </Box>
          </form>
        )}
      </Card>
    </MainLayout>
  );
}

export default Store;
