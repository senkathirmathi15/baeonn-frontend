import React, { useState } from "react";
import MainLayout from "../../layout/mainLayout";
import WrapperCard from "../../components/wrapperCard/wrapperCard";
import Stack from "@mui/material/Stack";
import BackupIcon from "@mui/icons-material/Backup";


import {
  makeStyles,
  Card,
  Grid,
  Button,
  Divider,
  TextField,
  Select,
  MenuItem,
  FormLabel,
  TextareaAutosize,
  Input,
  Typography,
  IconButton,
  FormControlLabel,
  Radio,
  RadioGroup,
  createMuiTheme,
  Box,
} from "@material-ui/core";
import { useNavigate } from "react-router-dom";
import { fontSize } from "@mui/system";
import Lable from "../../components/Typography/lable";
import { ConstructionOutlined } from "@mui/icons-material";
import {inviteStore} from "../../service config/admin.service";
import { ToastContainer, toast } from "react-toastify";

const useStyles = makeStyles((theme) => ({
  card: {
    maxWidth: "100%",
    boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
    minHeight: "100vh",
    // margin: "auto",
    marginTop: "100px",
    borderRadius: "5px",
    padding: "20px",
    display: "flex",
    alignItems: "start",
    justifyContent: "start",
  },
  invitebtn: {
    background: "#1456db",
    color: "white",
    fontWeight: "700",
    "&:hover": {
      background: "#1456db",
      color: "white",
    },
  },
  inputfile: {
    display: "none",
  },
  upload: {
    background: "#f0f3f5",
    display: "flex",
    flexDirection: "column",
    width: "100%",
    height: "100%",
    justifyContent: "center",
    borderRadius:"none"
  },
  // icon: {
  //   transform: "rotate(180deg)",
  // },
}));

export default function BulkInvite() {
  const classes = useStyles();
  let navigate = useNavigate();
  const[emails,setEmails]=useState();
  const[file,setFile]=React.useState();
  const onChange = (e) => {
    
    setFile(e.target.files[0]);
  };
  const onSubmit = async (e) => {
    e.preventDefault();
   
    console.log(emails)
    const emailarr = emails.split(",");
    console.log(emailarr)
    let data = {
      storeType:"chain",
      emails:emailarr,
    }
    const res = await inviteStore(data);
    if (!res.error) {
      toast.success(res.msg);
      navigate("/store") 
     
    } else {
      toast.error(res.msg);
    }
      
    
  };

  // const Api = (data)=>{
  //    console.log(data)

  // }

  const handelChange=(e)=>{
    console.log(e.target.value)
    setEmails(e.target.value)
  }

  return (
    <MainLayout>
      <form onSubmit={onSubmit} >
      <Card variant="outlined" className={classes.card}>
        <Grid container>
          <Grid item sm={12} md={6}>
            <Stack spacing={2} padding={3}>
              <Lable
                style={{ my: 2, display: "block" }}
                title="Invite Store Managers"
              />
              <Box sx={{ maxWidth: "1000px", }}>
                <Lable
                  style={{ my: 2, display: "block" }}
                  title="Enter Email"
                />
                <TextareaAutosize
                  aria-label="Enter Email's"
                  minRows={7}
                  placeholder="Enter Email's"
                  style={{ width: "100%" }}
                  fullWidth
                  value={emails}
                  onChange={handelChange}
                />
              </Box>
              <Lable
                style={{ my: 2, display: "block" }}
                title=" Invite Multiple Store Managers by separating Email IDs with a Comma."
              />
              <Lable
                style={{ my: 2, display: "block", color: "red" }}
                title="Eg: xyz@gmail.com, abc@gmail.com"
              />
              <Button type="submit" className={classes.invitebtn}>Send Invite</Button>
            </Stack>
            
          </Grid>
          
          
          {/* <Divider orientation="vertical" flexItem/> */}
          
        
          {/* <Grid item sm={12} md={6} padding={7}>
          <Stack spacing={2} padding={3}>
          <Lable
                style={{ my: 2, display: "block" }}
                title="Upload Excel"
              />
              
                          <TextField
                          variant="outlined"
                            accept="image/*"
                            id="storeimage"
                            type="file"
                            // className={classes.inputfile}
                            onChange={onChange}
                          />
                         
                           
                        <Button type="submit"className={classes.invitebtn}>upload</Button>
               </Stack>
          </Grid> */}
         
        </Grid>
      </Card>
      </form>
    </MainLayout>
  );
}
