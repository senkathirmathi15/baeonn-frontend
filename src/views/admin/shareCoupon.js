import React from "react";
import MainLayout from "../../layout/mainLayout";
import {
  makeStyles,
  Grid,
  Box,
  IconButton,
  TextField,
  Select,
  MenuItem,
  FormLabel,
  TextareaAutosize,
  Input,
  FormControlLabel,
  Radio,
  RadioGroup,
  createMuiTheme,
  Typography,
} from "@material-ui/core";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import CardShareCoupons from "../../components/Cards/CardShareCoupon";
import moment from "moment";
import { useSelector, useDispatch } from "react-redux";
import {
  getUserStore,
  getStoreCampaign,
  shareCoupon,
} from "../../service config/admin.service";
import { ToastContainer, toast } from "react-toastify";

const useStyles = makeStyles((theme) => ({
  cards: {
    marginTop: "100px",
  },
  submitBtn: {
    background: "blue !important",
    color: "white  !important",
    fontWeight: "700  !important",
    "&:hover": {
      background: "blue  !important",
      color: "white  !important",
    },
  },
  paper: { width: "900px" },
}));

export default function ShareCoupons() {
  const classes = useStyles();

  const [openModal, setOpenModal] = React.useState(false);
  const [selected, setSelected] = React.useState({});

  const handleClickOpen = () => {
    setOpenModal(true);
  };

  const handleClose = () => {
    setOpenModal(false);
  };
  const [maxWidth, setMaxWidth] = React.useState("lg");
  const [email, setEmail] = React.useState("");
  const [phone, setPhone] = React.useState("");
  const [phoneFocus, setPhoneFocus] = React.useState(false);
  const [emailFocus, setEmailFocus] = React.useState(false);

  const viewCampaign = useSelector(
    (state) => state.campaignDetails.viewCampaign.viewDetails
  );
  const outletName = useSelector(
    (state) => state.campaignDetails.viewCampaign.outletName
  );
  const id = useSelector((state) => state.campaignDetails.viewCampaign.storeId);
  const [rows, setRows] = React.useState([]);
  React.useEffect(() => {
    getUserStore();
  }, []);
  React.useEffect(() => {
    makeRows(viewCampaign);
  }, [viewCampaign]);

  function makeRows(cam) {
    const row = [];
    for (let i = 0; i < cam.length; i++) {
      row.push({
        sno: i,
        id: cam[i].id,
        created: moment(cam[i].created).format("DD/MM/YYYY"),
        title: cam[i].title,
        description: cam[i].description,
        numberOfCoupons: cam[i].noOfCoupons,
        shared: cam[i].noOfCouponsShared,
        notshared: cam[i].noOfCoupons - cam[i].noOfCouponsShared,
        paymentstatus: cam[i].paymentStatus,
        action: () => {
          console.log("KFC");
        },
      });
    }
    // console.log('row only', row)
    setRows(row);
  }

  const handleSubmit = async (e) => {
  
    e.preventDefault();
    if (!email && !phone) {
      toast.error("Any one email or phone is required");
      return;
    }
    const res = await shareCoupon({
      id: selected.id,
      data: {
        storeName: outletName,
        email: email,
        phone: phone,
      },
    });
    if (!res.error) {
      toast.success(res.payload);
    } else {
      toast.error(res.payload);
    }
  };
  const focusChange = (type)=>{
     switch (type) {
       case "email":
        setPhoneFocus(true);
        setEmailFocus(false);
        break;
        case "phone":
          setPhoneFocus(false);
        setEmailFocus(true)
         break;
     
       default:
         break;
     }

  }

  return (
    <>
      <MainLayout>
        <div className={classes.cards}>
          <CardShareCoupons
            closeModal={handleClose}
            setShowModal={handleClickOpen}
            rows={rows}
            setSelected={setSelected}
          />
        </div>

        <Dialog
          open={openModal}
          onClose={handleClose}
          maxWidth="900px"
          classes={{ paper: classes.paper }}
        >
          <form onSubmit={handleSubmit}>
            <DialogTitle>Share</DialogTitle>

            <DialogContent
              sx={{ textAlign: "center", maxWidth: "500px", margin: "0 auto" }}
            >
              <Typography variant="h5" display="block">
                Thanks for buying at
              </Typography>
              <Typography variant="h2" display="block">
                mcDolland
              </Typography>
              <Box
                component="h1"
                sx={{
                  outline: "5px dashed green",
                  padding: "1px",
                  background: "#90EE90",
                }}
              >
                1246756756
              </Box>
              <Box component="h3" sx={{ padding: "1px", color: "#90EE90" }}>
                Please use this coupon code to claim your reward
              </Box>
              <Box
                component="h3"
                sx={{
                  padding: "10px",
                  color: "#90EE90",
                  margin: 0,
                  padding: 0,
                }}
              >
                20% discount on starbucks
              </Box>
              <Box
                component="p"
                sx={{
                  padding: "1px",
                  color: "#90EE90",
                  margin: 0,
                  padding: 0,
                }}
              >
                phoneix market city,chennai
              </Box>
              <Box
                component="p"
                sx={{
                  padding: "1px",
                  color: "#90EE90",
                  margin: 0,
                  padding: 0,
                }}
              >
                Description goes here Lorem Ipsum has been the industry's
                standard dummy text ever since the 1500s, when an unknown
                printer took a galley of type and scrambled it to make a type
                specimen book.
              </Box>
              <Grid container alignContent="center">
                <Grid item sm={12} md={5}>
                <Typography
                variant="overline"
                display="block"
                sx={{
                  fontSize: "12px",
                  lineHeight: "1rem",
                  fontWeight: "bold",
                  color: "#1c1c1c",
                  marginBottom: "10px",
                }}
                
              >
                Email
              </Typography>
              <TextField
                id="outlined-basic"
                variant="outlined"
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                className={classes.input}
                InputProps={{ classes: { underline: classes.input } }}
                sx={{
                  border: "none",
                }}
                // disabled={emailFocus?emailFocus:false}
              onFocus={()=>focusChange("email")}
              />

                </Grid>
                <Grid item sm={12} md={2} > or</Grid>
               
                <Grid item sm={12} md={5}>
                
                <Typography
                  variant="overline"
                  display="block"
                  sx={{
                    fontSize: "12px",
                    lineHeight: "1rem",
                    fontWeight: "bold",
                    color: "#1c1c1c",
                    marginBottom: "10px",
                  }}
                >
                  Phone number
                </Typography>
                <TextField
                // disabled={phoneFocus?phoneFocus:false}
                  id="outlined-basic"
                  variant="outlined"
                  type="number"
                  value={phone}
                  onChange={(e) => setPhone(e.target.value)}
                  className={classes.input}
                  InputProps={{ classes: { underline: classes.input } }}
                  sx={{
                    border: "none",
                  }}
                  // onFocus={()=>focusChange("phone")}
                />
              

                </Grid>


              </Grid>

              

              
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} className={classes.submitBtn}>
                close
              </Button>
              <Button type="submit" className={classes.submitBtn}>
                Share
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </MainLayout>
    </>
  );
}
