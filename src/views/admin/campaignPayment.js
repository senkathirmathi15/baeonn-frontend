import React from "react";
import MainLayout from "../../layout/mainLayout";
import {
  makeStyles,
  Card,
  Grid,
  Box,
  Button,
  Typography,
  IconButton,
  TextField,
  Select,
  MenuItem,
  FormLabel,
  TextareaAutosize,
  Input,
  FormControlLabel,
  Radio,
  RadioGroup,
  createMuiTheme,
} from "@material-ui/core";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import Divider from "@mui/material/Divider";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import { textAlign } from "@mui/system";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { useSearchParams } from "react-router-dom";
import { paymentInfo } from "../../service config/admin.service";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { put, post } from "../../service config/http.service";
import { Ippopay } from "react-ippopay";

import { ToastContainer, toast } from "react-toastify";

const useStyles = makeStyles((theme) => ({
  cards: {
    marginTop: "100px",
  },
  card: {
    width: "100%",
    padding: "50px",
    margin: "0 auto",
  },
  btn: {
    background: "white",
    color: "blue",
    border: "1px solid blue",

    "&:hover": {
      background: "blue",
      color: "white",
    },
  },
  paybtn: {
    background: "blue",
    color: "white",

    "&:hover": {
      background: "blue",
      color: "white",
    },
  },
}));

export default function Payment() {
  const classes = useStyles();
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const [paymentDetail, setPaymentDetail] = React.useState();
  const [isCash, setIsCash] = React.useState(true);
  // const [payStart, setPayStart] = React.useState(false);
  const [paymentInit, setPaymentInit] = React.useState();
  const [ippopay, setIppoPay] = React.useState({
    ippopayOpen: false,
    order_id: "",
    public_key: "",
  });
  const [selectedPay, setSelectedPay] = React.useState();

  const [isCreditSelected, setIsCreditSelected] = React.useState(true);
  const [isPaid, setIsPaid] = React.useState(true);
  const dispatch = useDispatch();
  const payment = useSelector((state) => state.paymentDetail.paymentDetail);

  const toggleCredit = () => {
    setIsCreditSelected(!isCreditSelected);
    setIsCash(!isCash);
  };
  const payDetails = async (data) => {
    const response = await paymentInfo(data);
    if (!response.data.error) {
      toast.success("Please Complete the payment to make your campaign live");
      toast.info("You can complete pending payment from View Campaigns page");
      setIsPaid(false);
    } else {
      toast.error(response.data.payload);
      navigate("/");
    }
  };

  React.useEffect(() => {
    const campaignId = searchParams.get("id");

    payDetails({
      campaignId: campaignId,
    });
    window.addEventListener("message", ippopayHandler);
    return () => {
      setIppoPay({
        ippopayOpen: false,
        order_id: "",
        public_key: "",
      });
    };
  }, []);

  const initiatePayment = (e) => {
    e.preventDefault();
    const campaignId = searchParams.get("id");
    let paymentData = {};
    if (isCash) paymentData = payment.fee.withoutCredit;
    else paymentData = payment.fee.withCredit;
    setSelectedPay(paymentData);
    post(`/campaign/${campaignId}/pay`, {
      campaignId,
      paymentData,
      advertiserId: payment.campaign.advertiserId,
    })
      .then((response) => {
        setPaymentInit(response.data.payload.order);
        setIppoPay({
          ippopayOpen: true,
          order_id: response.data.payload.data.order.order_id,
          public_key: response.data.payload.data.order.public_key,
        });
      })
      .catch((e) => {
        toast.error(e.response.data.payload);
      });
  };

  const ippopayHandler = (e) => {
    const campaignId = searchParams.get("id");
    if (e.data.status == "success") {
      toast.success(e.data.status);
      put(`/campaign/${campaignId}/pay`, {
        gatewayData: e.data,
        campaignId,
        paymentData: selectedPay,
        advertiserId: payment?.campaign?.advertiserId,
      })
        .then((response) => {
          toast.success(response.data.payload);
        })
        .catch((e) => {
          toast.success(e.response.data.payload);
        });
    }
    if (e.data.status == "failure") {
      toast.error(e.data.status);
    }
  };

  const CampaignCostWithoutCredit = ({ pay }) => {
    // const b = paymentDetail?.fee?.withoutCredit?.pay;
    const { cash, gst, credit, total } = pay;
    return (
      <TableContainer>
        <Table
          sx={{
            maxWidth: 400,
            margin: "0 auto",
            boxShadow: "0px 10px 15px -3px rgba(0,0,0,0.1)",
          }}
        >
          <TableBody>
            <TableRow>
              <TableCell component="th" scope="row">
                BAEON Fee
              </TableCell>
              <TableCell
                align="center"
                sx={{ background: "#c2c0c0", color: "white" }}
              >
                {cash}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                GST(18%)
              </TableCell>
              <TableCell
                align="center"
                sx={{ background: "#c2c0c0", color: "white" }}
              >
                {gst}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell component="th" scope="row">
                Credit{" "}
                <Button
                  variant="outlined"
                  size="small"
                  className={classes.btn}
                  onClick={toggleCredit}
                >
                  Include credit
                </Button>
              </TableCell>
              <TableCell
                align="center"
                sx={{ background: "#c2c0c0", color: "white" }}
              >
                {credit}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row" sx={{ fontSize: "25px" }}>
                Total
              </TableCell>
              <TableCell
                align="center"
                sx={{ background: "#c2c0c0", color: "white", fontSize: "25px" }}
              >
                {total}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    );
  };

  const CampaignCostWithCredit = ({ pay }) => {
    // const a = paymentDetail?.fee?.withCredit?.pay;
    return (
      <TableContainer>
        <Table
          sx={{
            maxWidth: 400,
            margin: "0 auto",
            boxShadow: "0px 10px 15px -3px rgba(0,0,0,0.1)",
          }}
        >
          <TableBody>
            <TableRow>
              <TableCell component="th" scope="row">
                BAEON Fee
              </TableCell>
              <TableCell
                align="center"
                sx={{ background: "#c2c0c0", color: "white" }}
              >
                {pay && pay.cash}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                GST(18%)
              </TableCell>
              <TableCell
                align="center"
                sx={{ background: "#c2c0c0", color: "white" }}
              >
                {pay && pay.gst}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell component="th" scope="row">
                Credit{" "}
                <Button
                  variant="outlined"
                  size="small"
                  className={classes.btn}
                  onClick={toggleCredit}
                >
                  Exclude Credit
                </Button>
              </TableCell>
              <TableCell
                align="center"
                sx={{ background: "#c2c0c0", color: "white" }}
              >
                {pay && pay.credit}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row" sx={{ fontSize: "25px" }}>
                Total
              </TableCell>
              <TableCell
                align="center"
                sx={{ background: "#c2c0c0", color: "white", fontSize: "25px" }}
              >
                {pay && pay.total}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    );
  };

  const PayComp = () => {
    return (
      <div>
        {isCreditSelected ? (
          <CampaignCostWithCredit pay={payment?.fee?.withCredit?.pay} />
        ) : (
          <CampaignCostWithoutCredit pay={payment?.fee?.withoutCredit?.pay} />
        )}
      </div>
    );
  };

  return (
    <>
      <MainLayout>
        <div className={classes.cards}>
          <Card variant="outlined" className={classes.card}>
            <PayComp></PayComp>

            <Box sx={{ textAlign: "center", marginTop: "30px" }}>
              <div></div>
              <Button
                size="large"
                className={classes.paybtn}
                onClick={(e) => initiatePayment(e)}
              >
                Proceed to pay
              </Button>
              <Ippopay
                ippopayOpen={ippopay.ippopayOpen}
                ippopayClose={true}
                order_id={ippopay.order_id}
                public_key={ippopay.public_key}
                style={{ cursor: "pointer" }}
              />
            </Box>
          </Card>
        </div>
      </MainLayout>
    </>
  );
}
