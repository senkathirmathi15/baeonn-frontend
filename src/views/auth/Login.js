import React from "react";
import Typography from "@mui/material/Typography";
import FormControl, { useFormControl } from "@mui/material/FormControl";
import OutlinedInput from "@mui/material/OutlinedInput";
import Box from "@mui/material/Box";
import FormHelperText from "@mui/material/FormHelperText";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import AuthLayout from "../../layout/authlayout";
import { useNavigate } from "react-router-dom";
import Stack from "@mui/material/Stack";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Link from "@mui/material/Link";
import {
  Link as RouterLink,
  LinkProps as RouterLinkProps,
  MemoryRouter,
} from "react-router-dom";

export default function BusinessDetail() {
  let navigate = useNavigate();
  const handleSubmit = (event) => {
    event.preventDefault();
    navigate(`/login`);
  };
  return (
    <>
    
      <AuthLayout>
        <Box
          component="form"
          noValidate
          autoComplete="off"
          sx={{ mb: 2 }}
          onSubmit={handleSubmit}
        >
          <Typography
            variant="lable"
            color="text.secondary"
            align="start"
            sx={{ my: 2, display: "block" }}
          >
            Email
          </Typography>
          <TextField
            variant="outlined"
            fullWidth
            placeholder="Email"
            name="email"
          />
          <Typography
            variant="lable"
            sx={{ my: 2, display: "block" }}
            color="text.secondary"
            align="start"
          >
            Password
          </Typography>
          <TextField
            variant="outlined"
            fullWidth
            placeholder="Password"
            name="email"
          />
          <Box textAlign={"end"}>
            <Link component={RouterLink} to="/store">
              forgotPassword?
            </Link>
          </Box>
          <FormControlLabel
            control={<Checkbox defaultChecked />}
            label="Remember me"
          />

          <Button
            type="submit"
            fullWidth
            variant="contained"
            style={{ fontSize: "18px" }}
            sx={{ mt: 3 }}
          >
            Submit
          </Button>
          <Button
            component={RouterLink}
            to="/register"
            variant="outlined"
            fullWidth
            sx={{ mt: 3 }}
          >
            Create new account
          </Button>
        </Box>
      </AuthLayout>
    </>
  );
}
