import React, { useEffect } from "react";
import Typography from "@mui/material/Typography";
import FormControl, { useFormControl } from "@mui/material/FormControl";
import OutlinedInput from "@mui/material/OutlinedInput";
import Box from "@mui/material/Box";
import FormHelperText from "@mui/material/FormHelperText";
import Button from "@mui/material/Button";
import { ToastContainer, toast } from "react-toastify";

import TextField from "@mui/material/TextField";
import AuthLayout from "../../layout/authlayout";
import { useNavigate } from "react-router-dom";
import Lable from "../../components/Typography/lable"
import { useSelector, useDispatch } from "react-redux";
import { businessOwnerReducer } from "../../service config/configStore";
import {
  submitBusinessOwnerDetails
} from "../../service config/admin.service";

export default function BusinessDetail() {
  const businessOwnerDetails = useSelector((state) => state.loginDetail.businessOwnerDetails);
  const dispatch = useDispatch();
  let navigate = useNavigate();
  const handleSubmit = async (event) => {
    event.preventDefault();
    const checkEmail =/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(businessOwnerDetails.businessOwnerEmail);
    if(checkEmail && businessOwnerDetails.businessName!==""&& businessOwnerDetails.businessOwnerFirstName!==""&& businessOwnerDetails.businessOwnerLastName!==""){
    const response = await submitBusinessOwnerDetails(businessOwnerDetails);
    
    if (!response.error) {
      toast.success(response.msg);
      if(response.msg === "Success"){
        navigate(`/create-store`);
  }
      
    } else {
      toast.error(response.msg);
    }
  }
  else{
    toast.error("please check emailId");
  }
    // navigate(`/store`);
  };
  const handleChange = (name,value)=>{
    const inputValue = value;
      dispatch(businessOwnerReducer({[name]:inputValue}));
    

 

  }
  useEffect(()=>{
    dispatch(businessOwnerReducer({
      businessName: "",
      businessOwnerFirstName: "",
      businessOwnerLastName: "",
      businessOwnerEmail: ""
    }));
  },[])
  return (
    <>
      <AuthLayout>
        <Box component="form" noValidate autoComplete="off" sx={{ mb: 2 }} onSubmit={handleSubmit}>
        
          <Lable style={{my: 1 ,display:"block"}} title="Registered Company Name"/>
           
            <TextField
              placeholder="Enter Registered Company Name"
              name="name"
              fullWidth
              required
              value={businessOwnerDetails.businessName}
              onChange={(e)=>handleChange("businessName",e.target.value)}
            />
            <Lable style={{my: 1 ,display:"block"}} title="Business Owner's FristName"/>
            <TextField
              placeholder="Enter Business Owner's Name"
              name="fristName"
              sx={{ my: 1,display:"block" }}
              fullWidth
              required
              value={businessOwnerDetails.businessOwnerFirstName}
              onChange={(e)=>handleChange("businessOwnerFirstName",e.target.value)}
            />
            <Lable style={{my: 1 ,display:"block"}} title="Business Owner's LastName"/>
            <TextField
              placeholder="Enter Business Owner's Name"
              name="lastName"
              required
              onChange={(e)=>handleChange("businessOwnerLastName",e.target.value)}
              sx={{ my: 1,display:"block" }}
              fullWidth
              value={businessOwnerDetails.businessOwnerLastName}
            />
              <Lable style={{my: 1 ,display:"block"}} title="Email ID"/>
           
            <TextField
              variant="outlined"
              fullWidth
              placeholder="Enter Email ID"
              name="email"
              required
              value={businessOwnerDetails.businessOwnerEmail}
              onChange={(e)=>handleChange("businessOwnerEmail",e.target.value)}
            />

            <Button
              type="submit"
              fullWidth
              variant="contained"
              style={{ fontSize: "18px" }}
              sx={{ mt: 3 }}
            >
              Submit
            </Button>
          </Box>
        
      </AuthLayout>
    </>
  );
}
