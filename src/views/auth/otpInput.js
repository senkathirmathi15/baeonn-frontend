import React, { useState,useEffect } from "react";
import OtpInput from "react-otp-input";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { useNavigate } from "react-router-dom";
import AuthLayout from "../../layout/authlayout";
import Link from "@mui/material/Link";
import { ToastContainer, toast } from "react-toastify";

import Box from "@mui/material/Box";
import Lable from "../../components/Typography/lable";
import { useSelector, useDispatch } from "react-redux";
import { genrateOtp,verifyOtp } from "../../service config/auth.service";
import LoadingBtn from "../../components/Button/loadingBtn"
import {getCurrentUser} from "../../service config/auth.service"
import {STOREOWNER} from "../../utilities/constants"

export default function Otp(props) {
  const mobileNumber = useSelector((state) => state.loginDetail.mobileNumber);
  const [otp, setOtp] = useState("");
  let navigate = useNavigate();
  const [loading,setLoading]=useState(false);

  useEffect(()=>{
     if(!mobileNumber){
      navigate("/");
     } 

  },[])

  const handleChange = (otp) => {
    setOtp(otp);
  };
  const handleSubmit = async(event) => {
    event.preventDefault();
    if (otp.length === 4) {
      try {
        setLoading(true)
        const response = await verifyOtp({mobileNumber,otp});
        if (!response.error) {
          toast.success(response.msg);
          if(response.msg === "Success"){
            setLoading(false)
            if(response.result.businessId){
              if(getCurrentUser().roleId === STOREOWNER ){
                navigate(`/create-store`);
              }
              else{
                navigate(`/store`);
              }
              
            }
            else{
              navigate(`/info`);
            }
                
          }
          
        } else {
          setLoading(false)
          toast.error(response.msg);
        }
      } catch (error) {
        setLoading(false)
        
      }
   
    }

   
  };

  const reSendOtp = async () => {
    if (mobileNumber !== "") {
      const response = await genrateOtp({mobileNumber});
      if (!response.error) {
        toast.success(response.msg);
      } else {
      
        toast.error(response.msg);
      }
    }
  };

  return (
    <>
      <AuthLayout>
        {/* <Typography
          variant="lable"
          color="text.secondary"
          align="center"
          sx={{ my: 2 }}
        >
          Enter OTP to verify your mobile number
        </Typography> */}
        <Lable
          style={{ my: 2, marginRight: "auto" }}
          title="Enter OTP to verify your mobile number"
        />
        <OtpInput
          value={otp}
          onChange={handleChange}
          numInputs={4}
          separator={<span></span>}
          isInputNum={true}
          inputStyle={{
            width: "30%",
            padding: "20px",
            borderRadius: "10px",
            border: "none",
            boxShadow: "10px 10px 5px 0px rgba(13,12,12,0.17)",
          }}
          focusStyle={{
            outline: "none",
          }}
        />
        <Box textAlign="center" py={3}>
          <Link onClick={reSendOtp}>Resend OTP</Link>
        </Box>

        <LoadingBtn
          type="submit"
          
          variant="contained"
          style={{ fontSize: "18px" }}
          sx={{ mt: 3 }}
          onClick={handleSubmit}
          disabled={otp.length === 4?false:true}
          name="Verify"
          loadingIndicator="verifying OTP..."
          loading={loading}
        >
          Verify
        </LoadingBtn>
      </AuthLayout>
    </>
  );
}
