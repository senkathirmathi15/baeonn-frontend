import {useEffect, useState}  from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";


import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import MuiPhoneNumber from "material-ui-phone-number";

import { useNavigate,useParams ,useSearchParams } from "react-router-dom";
import AuthLayout from "../../layout/authlayout";
import { ToastContainer, toast } from "react-toastify";
import Lable from "../../components/Typography/lable"
import {phoneRegExp} from "../../utilities/constants";
import { useSelector, useDispatch } from "react-redux";
import { loginReducer } from "../../service config/configStore";
import {
  genrateOtp
} from "../../service config/auth.service";
import LoadingBtn from "../../components/Button/loadingBtn"

export default function SignIn() {
  const mobileNumber = useSelector((state) => state.loginDetail.mobileNumber);
  const dispatch = useDispatch();
  let navigate = useNavigate();
  const [loading,setLoading]=useState(false);
  // let { token } = useParams();
  const [searchParams, setSearchParams] = useSearchParams();
  const invitationToken = searchParams.get("token")
  console.log(mobileNumber+"enter")
  
  const handleSubmit = async(event) => {
    event.preventDefault();
    console.log(mobileNumber)
   
    if(mobileNumber!==""){
      try {
        setLoading(true)
        const response = await genrateOtp({
          mobileNumber,
          ...(invitationToken && {invitationToken}),
        });
        console.log(response.error)
        if (!response.error) {
          setLoading(false)
          toast.success(response.msg);
      
          // console.log(response.payload+"response")
          navigate(`/otp`);
        } else {
          setLoading(false)
          toast.error(response.msg);
        }
      } catch (error) {
        setLoading(false)
      }
      
   
    }
    else{
      toast.error("Please check Phone number");

    }
  };
  function handleOnChange(value) {
    const number = `${value.replace(/[\. ,:-]+/g, "")}`
    dispatch(loginReducer({ mobileNumber:number}));
   
 }
 useEffect(()=>{
  dispatch(loginReducer({ mobileNumber:""}));
 },[])

  

  return (
    <>
      <AuthLayout>
    
        <Lable style={{my: 2 ,marginRight: "auto",}} title="Enter Mobile Number"/>
        <Box
          component="form"
          onSubmit={handleSubmit}
          noValidate
          
        >
          <MuiPhoneNumber
            name="phone"
            id="phone"
            placeholder="Enter"
            // data-cy="user-phone"
            defaultCountry={"in"}
            fullWidth
            variant="outlined"
            sx={{ mb: 4 }}
           
            onChange={handleOnChange}
          />
          <LoadingBtn
            type="submit"
            fullWidth
            variant="contained"
            loading={loading}
            loadingIndicator="sending OTP..."
            name="Send OTP"
            // style={{ fontSize: "18px" }}
          >
            Send OTP
          </LoadingBtn>
        </Box>
        
      </AuthLayout>
    </>
  );
}
