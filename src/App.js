import logo from "./logo.svg";
import "./App.css";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
import Register from "./views/auth/register";
import Login from "./views/auth/login";
import Container from "@mui/material/Container";
import { makeStyles, Card, Box } from "@material-ui/core";
import Otp from "./views/auth/otpInput";
import Image from "./assets/register_bg_2.png";
import Button from "@mui/material/Button";
import BusinessDetail from "./views/auth/businessDetail";
import Store from "./views/admin/store";
import Business from "./views/admin/business";
import ViewCampaigns from "./views/admin/viewCampaigns";
import ViewCoupon from "./views/admin/shareCoupon";
import CreateCampaign from "./views/admin/createCampaign";
import ClaimCoupons from "./views/admin/claimCoupon";
import ShareCoupons from "./views/admin/shareCoupon";
import CampaignPayment from "./views/admin/campaignPayment";
import CreditAndRef from "./views/admin/creditandref";
import PrimeCustomer from "./views/admin/primeCustomer";
import Dashboard from "./views/admin/dashboard";
import BulkInvite from "./views/admin/bulkInvite";
import { theme } from "../src/Theme/theme";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import Private from "./routes/private.routes";
import component from "./routes/admin.routes";

function App() {
  return (
    <>
      <ThemeProvider theme={theme}>
        <Router>
          <Routes>
            {/* add routes with components */}
            {/* <Route path="/" exact element={<Login />} /> */}
           
            <Route path="/" element={<Navigate replace to="/login" />} />
            <Route path="/login" exact element={<Register />} />
            <Route path="/otp" exact element={<Otp />} />
            {/* <Route path="/info" exact element={<BusinessDetail />} /> */}
            {/* private Routes */}
            <Route path="/" element={<Private />}>
              {component.map((item) => {
                return <Route path={item.path} exact element={item.element} />;
              })}
            </Route>
             {/* private Routes */}
          </Routes>
        </Router>
      </ThemeProvider>
    </>
  );
}

export default App;
