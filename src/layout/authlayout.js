import React from 'react'
import { makeStyles, Card, Box } from "@material-ui/core";
import Container from "@mui/material/Container";
import Image from "../assets/register_bg_2.png";
import Logo from "../assets/baeon-full.jpg";
import { ToastContainer, toast } from "react-toastify";



const useStyles = makeStyles((theme) => ({
  main: {
    backgroundImage: `url(${Image})`,
    position: "relative",
    backgroundSize: "cover",
    backgroundPosition: "center",
    background: "#141414",
  },
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    minHeight: "100vh",
  },
  content: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    minHeight: "100vh",
    // maxWidth: "500px",
    margin: "0 auto",
  },
  card: {
    maxWidth: "300px",
    minHeight: "300px",
    padding: "30px",
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "center",
  },
  imgDiv:{

    PaddingBottom:"30px"
  }
}));

export default function Layout(props) {
  
      const classes = useStyles();

  return (
    <div className={classes.main}>
      <Container component="main" maxWidth="md" className={classes.container}>
        <Box className={classes.content}>
          <Card variant="outlined" color="primary" className={classes.card}>
            <div className={classes.imgDiv}>
              <img src={Logo} width="200px" height="80px" alt="logo"/>
            </div>
            {props.children}
          </Card>
        </Box>
        <ToastContainer></ToastContainer>
      </Container>
    </div>
  );
}
