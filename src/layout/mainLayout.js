import * as React from "react";
import PropTypes from "prop-types";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import MailIcon from "@mui/icons-material/Mail";
import MenuIcon from "@mui/icons-material/Menu";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Image from "../assets/baeon-full.jpg";
import Profile from "../assets/logo.jpeg";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Hamburger from "hamburger-react";
import Popover from '@mui/material/Popover';


import {
  makeStyles,
  Card,
  Grid,
  Button,
  TextField,
  Select,
  MenuItem,
  FormLabel,
  TextareaAutosize,
  Input,
  FormControlLabel,
  Radio,
  RadioGroup,
  createMuiTheme,
} from "@material-ui/core";
import PhotoCamera from "@mui/icons-material/PhotoCamera";
import BackupIcon from "@mui/icons-material/Backup";
import HeightIcon from "@mui/icons-material/Height";

import Stack from "@mui/material/Stack";

import { borderRadius, height, textAlign } from "@mui/system";
import Link from "@mui/material/Link";
import {
  NavLink as RouterLink,
  LinkProps as RouterLinkProps,
  MemoryRouter,
  useLocation,
  useNavigate,
  useParams,
} from "react-router-dom";
import SettingsApplicationsIcon from "@mui/icons-material/SettingsApplications";
import SiderBar from "../components/sideBar/sidebar"

const drawerWidth = 270;
const theme = createMuiTheme({
  overrides: {
    MuiInput: {
      underline: {
        "&:hover:not($disabled):before": {
          backgroundColor: "rgba(0, 188, 212, 0.7)",
        },
      },
    },
  },
});

const useStyles = makeStyles((theme) => ({
  image: {
    borderRadius: "3%",
  },
  caption: {
    fontSize: "0.75rem",
    lineHeight: "1rem",
    fontWeight: "bold",
    color: "#57748a",
  },
  card: {
    maxWidth: "1000px",
    boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
    minHeight: "100vh",
    margin: "0px auto",
    marginTop: "100px",
    borderRadius: "5px",
    padding: "20px",
  },
  btn: {
    background: "white",
    color: "blue",
    fontWeight: "700",
    "&:hover": {
      background: "white",
      color: "blue",
    },
  },
  inputfile: {
    display: "none",
  },
  upload: {
    background: "#f0f3f5",
    display: "flex",
    flexDirection: "column",
    width: "100%",
    height: "100%",
    justifyContent: "center",
  },
  icon: {
    transform: "rotate(180deg)",
  },
  grid: {
    padding: "25px 0",
  },
  profile: {
    width: "50px",
    height: "50px",
    borderRadius: "50%",
  },
  input: {
    border: "none",
    boxShadow: "0 3px 5px rgb(0 0 0 / 0.1)",
    background: "white",
    "&:hover": {
      border: "none",
      outline: "none",
    },
  },
  isactive: {
    color: "red",
    background: "green",
  },
}));

function ResponsiveDrawer(props) {
  const { window } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [isOpen, setOpen] = React.useState(false);
  const [islogout, setlogout] = React.useState(false);
  const location = useLocation();
  const classes = useStyles();
  const navigate = useNavigate();

  const logOut= ()=>{
    localStorage.removeItem('baeon_user');
    navigate("/login");

  }
 

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />

      <Box
        component="nav"
        sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
        aria-label="mailbox folders"
      >
        <Drawer
          variant="permanent"
          sx={{
            display: { xs: "none", sm: "block" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
          open
        >
          <SiderBar/>
        </Drawer>
      </Box>
      <Box
        component="main"
        className={classes.main}
        sx={{
          flexGrow: 1,
          p: 3,
          width: { sm: `calc(100% - ${drawerWidth}px)` },
          background: "#e6f4f5",
          minHeight: "100vh",
          position: "relative",
        }}
      >
        <Box
          sx={{
            width: "100%",
            background: "rgba(2, 132, 199,1)",
            position: "absolute",
            height: "300px",
            top: "0",
            left: "0",
            zIndex: "0",
          }}
        />
        <Box
          sx={{
            position: "relative",
            zIndex: "1",
            width: "100%",
            minHeight: "100vh",
          }}
        >
          <div>
            <Box
              sx={{
                display: { xs: "block", sm: "none" },
              }}
            >
              <Hamburger toggled={isOpen} toggle={setOpen} />
              {isOpen && (
                <>
                  <Box
                    sx={{
                      display: { xs: "block", sm: "none" },
                      position: "absolute",
                      top: "10",
                      zIndex: "20",
                      background: "white",
                      "& .MuiDrawer-paper": {
                        boxSizing: "border-box",
                        width: drawerWidth,
                      },
                    }}
                  >
                   <SiderBar/>
                  </Box>
                </>
              )}
            </Box>

            <Grid container spacing={2} columns={{ xs: 4, md: 12 }}>
              <Grid item xs={4}>
                <Box display="flex" alignItems="center" justifyContent="start">
                  <Button className={classes.btn} component={RouterLink} to="/claimCoupons">CLAIM COUPONS</Button>
                </Box>
              </Grid>

              <Grid item xs={4} justifyContent="center">
                <Box display="flex" alignItems="center" justifyContent="center">
                  <Button className={classes.btn} component={RouterLink} to="/shareCoupons">SHARE COUPONS</Button>
                </Box>
              </Grid>
              
              <Grid item xs={4} justifyContent="end">
                <Box display="flex" alignItems="end" justifyContent="end" flexDirection="column"  >
                  <img
                    src={Profile}
                    alt="Profile"
                    className={classes.profile}
                    onClick={()=>setlogout(!islogout)}
                  />
                  {islogout && < Paper sx={{padding:"10px 30px",marginTop:"5px"
                }} onClick={logOut}>Logout</Paper>}
               </Box>
              </Grid>
            </Grid>
          </div>

          {props.children}
        </Box>
      </Box>
    </Box>
  );
}

export default ResponsiveDrawer;
