import { FILE_UPLOAD } from "../service config/http.service";
import { getCurrentUser } from "../service config/auth.service";
import { put, get,post,patch } from "../service config/http.service";
import { ToastContainer, toast } from "react-toastify";
import { useSelector, useDispatch } from "react-redux";
import { store, businessDetailReducer,bankDetailReducer,creditReducer,storeReducer,viewCampaignReducer,mediaPartnerReducer,addPaymentReducer,multiplestoresReducer,invitesStoresReducer } from "../service config/configStore";
export const submitBusinessOwnerDetails= async (data) => {
  try {
    const res = await post(`/users/${getCurrentUser().roleId}/business`, {...data});
    return res.data;
  } catch (e) {
    return e.response.data;
  }
};
export const submitBusinessDetail = async (data) => {
  try {
    const res = await put(`/user/${getCurrentUser().id}/business-detail`, {...data});
    return res.data;
  } catch (e) {
    return e.response.data;
  }
};


export const submitBankDetail = async (data) => {
  try {
    const res = await put(`/user/${getCurrentUser().id}/bank-detail`, {...data});
    return res.data;
  } catch (e) {
    return e.response.data;
  }
};


export const updateStoreSubmit = async (data) => {
  try {
    const res = await put(`/store/${data.id}`, data.store);
    return res.data;
  } catch (e) {
    return e.response.data;
  }
};
export const getStoreDetail = () => {
  get(`/stores`).then((res) => {
    store.dispatch(multiplestoresReducer(res.data.result));
  });
};
export const getStoreDetailforManager = (data) => {
  get(`/stores/${data.id}`).then((res) => {
    store.dispatch(multiplestoresReducer([res.data.result]));
  });
};
export const getInviteStore = () => {
  get(`/stores/invites`).then((res) => {
    store.dispatch(invitesStoresReducer(res.data.result));
  });
};


export const reSendInviteStore = async (data) => {
  try {
    const res = await patch(`stores/resend-invites`, data.body);
    return res.data;
  } catch (e) {
    
    return e.response.data;
  }
};
export const deleteSendInviteStore = async (data) => {
  try {
    const res = await patch(`stores/delete-invites`, data.body);
    return res.data;
  } catch (e) {
    
    return e.response.data;
  }
};
export const getStoreDetailById = (data) => {
  get(`/stores/${data.id}`).then((res) => {
    store.dispatch(storeReducer({...res.data.result}));
  });
};

export const updateStoreDetailById = async (data) => {
  try {
    const res = await put(`/stores/${data.id}`, data.body);
    return res;
  } catch (e) {
    
    return e.response.data;
  }
};
export const submitStoreDetail = async (data) => {
 console.log("enter")
  try {
    const res = await post(`/stores`, data);
    return res.data;
  } catch (e) {
    return e.response.data;
  }
};
export const getBussinessDetail = () => {
  get(`/user/${getCurrentUser().id}/business-detail`).then((res) => {
    store.dispatch(businessDetailReducer({ ...res.data.payload }));
  });
};
export const getBankDetail = () => {
  get(`/user/${getCurrentUser().id}/bank-detail`).then((res) => {
    store.dispatch(bankDetailReducer({ ...res.data.payload }));
  });
};
export const getCredit = () => {
    get(`/user/${getCurrentUser().id}`)
    .then(result => {
        console.log(result.data.payload)
        store.dispatch(creditReducer({...result.data.payload }));
    }).catch(err => {
        console.log(err);
    })
};

export const getUserStore = () => {
  return get(`/user/${getCurrentUser().id}/store`)
  .then(response => {
    console.log(response.data.payload.outletName+"dfsdfsd")
    store.dispatch(viewCampaignReducer({storeId:response.data.payload.id,outletName:response.data.payload.outletName }));
    if(response.data.payload.id){
      getStoreCampaign(response.data.payload.id)
    }
  }).catch(err => {
    console.log(err.response);
  })
} 

export const getStoreCampaign = (storeId) =>{
  return get(`/store/${storeId}/campaign-own`)
  .then(response => {
    store.dispatch(viewCampaignReducer({viewDetails:response.data.payload }));
  }).catch(err => {
    // console.log(err.response)
  })
}

export const shareCoupon = async (data) => {
  try {
    const res = await post(`/campaign/${data.id}/share-coupon`, {...data.data});
    return res.data;
  } catch (e) {
    return e.response.data;
  }
};
export const claimCoupon = async (data) => {
  try {
    const res = await post(`/coupon/claim`, {...data});
    return res;
  } catch (e) {
   
    return e.response;
  }
};
export const createCampaign = async (data) => {
  try {
    const res = await post(`/campaign`, {...data});
    return res;
  } catch (e) {
    return e.response;
  }
};
export const getMediaPartner = async () => {
  try {
    const res = await  get(`/media-partner`);
    store.dispatch(mediaPartnerReducer(res.data.payload));
    return res;
  } catch (e) {
    
    return e.response;
  }
};

export const paymentInfo = async (data)=>{
  try {
    const res= await post(`/campaign/${data.campaignId}/pay-detail`,{...data})
    store.dispatch(addPaymentReducer(res.data.payload));
    return res;
  }catch (e) {
    
    return e.response;
  }
}
export const inviteStore = async (data)=>{
  try {
    const res= await post(`stores/invite`,{...data})
    return res.data;
  }catch (e) {
    
    return e.response.data;
  }
}


// --------------------------------------inputfieldError-------------------------------------------------

export const validateInputs = (obj) => {
  const items = Object.keys(obj);
  const errors = {};
  items.map((key) => {
    const value = obj[key];
    if (!value) {
      errors[key] = key + " is required";
    }
  });
  return errors;
};

export const findErrors = (obj) => {
  const items = Object.keys(obj);
  const errors = [];
  items.map((key) => {
    const value = obj[key];
    if (!value) {
      errors.push(key);
    }
  });
  // console.log("errors", errors);
  if (errors.length > 0) {
    return true;
  } else {
    return false;
  }
};



