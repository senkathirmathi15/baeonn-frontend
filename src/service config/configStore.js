import { createSlice, configureStore   } from '@reduxjs/toolkit'
export const login = createSlice({
  name: 'login',
  initialState: {
    mobileNumber:"",
    businessOwnerDetails :{
      businessName: "",
      businessOwnerFirstName: "",
      businessOwnerLastName: "",
      businessOwnerEmail: ""
    }
  },
  reducers: {
    loginReducer: (state, action) => {
     
      state.mobileNumber =action.payload.mobileNumber
    },
    businessOwnerReducer:(state, action)=>{
      state.businessOwnerDetails={...state.businessOwnerDetails,...action.payload}

    }
   
   
    
  }
})
export const slice = createSlice({
  name: 'settings',
  initialState: {
    businessDetail : {gstExempted: true, state : '', gstNumber : '', gstUrl : ''},
    bankDetail :{panNumber: '', accountHolderName: '', accountNumber: '', bankName: '', ifscCode: '', kycDocument : ''},
  },
  reducers: {
    businessDetailReducer: (state, action) => {
      state.businessDetail = {...state.businessDetail,...action.payload}
    },
    bankDetailReducer: (state, action) => {
      state.bankDetail = {...state.bankDetail,...action.payload}
    },
   
    
  }
})
export const credit = createSlice({
    name: 'credits',
    initialState: {
       ref:{ credit:0,
             refCode:""}
    },
    reducers: {
    creditReducer: (state, action) => {
        state.ref = {...state.ref,...action.payload}
        
      },
    }
});
export const storeDetail = createSlice({
    name: 'store',
    initialState: {
      storeDetail:{outletName : '', category: '', locality: '', address:{street:"", state: '', city: '',zipcode:""},
       email: '', contactNumber: '', customerCareNumber: '', numberOfWalkIns : 0, averageBillValue : 0, imageUrl: '', logoUrl: '', workingHours: [],id:"",type:"chain" },
       
    },
    stores:[],
    invites:[],
    reducers: {
     storeReducer: (state, action) => {
        state.storeDetail = {...state.storeDetail,...action.payload}
        
      },
     multiplestoresReducer: (state, action) => {
        state.stores = [...action.payload]
      },
     invitesStoresReducer: (state, action) => {
      console.log(action.payload)
        state.invites = [...action.payload]
      },
    }
});
export const campaignDetails = createSlice({
    name: 'campaign',
    initialState: {
       viewCampaign:{
            outletName:null,
              storeId:null,
              viewDetails:[]
       }    
    },
    reducers: {
      viewCampaignReducer: (state, action) => {
        state.viewCampaign = {...state.viewCampaign,...action.payload}
      },
    }
});
export const campaignAdd = createSlice({
    name: 'campaignAdd',
    initialState: {
      addCampaign:{
        title: '', description: '', noOfCoupons: 100, extraText: '',
        startCampaign: '', endCampaign: '', couponEndDate: '', advertiserId: '', mediaPartnerId: '',
        creditPayment: false, cashPayment: true, couponImage: '', linkToApp: '', linkToWebsite : '', mediaPartnerName: ''},
        mediaPartner:[]
          
    },
    reducers: {
      addCampaignReducer: (state, action) => {
        state.addCampaign = {...state.addCampaign,...action.payload}
      },
      mediaPartnerReducer:(state,action)=>{
        state.mediaPartner = [...action.payload]
      }
    }
});
export const paymentDetail = createSlice({
         name:"paymentDetails",
         initialState: {
         paymentDetail:{}
              
        },
        reducers: {
          addPaymentReducer: (state, action) => {
            state.paymentDetail = {...state.paymentDetail,...action.payload}
          },
        }
})

export const { businessDetailReducer,bankDetailReducer } = slice.actions;
export const { creditReducer } = credit.actions;
export const { storeReducer,multiplestoresReducer,invitesStoresReducer } = storeDetail.actions;
export const { viewCampaignReducer } = campaignDetails.actions;
export const { addCampaignReducer,mediaPartnerReducer } = campaignAdd.actions;
export const { addPaymentReducer } = paymentDetail.actions;
export const { loginReducer,businessOwnerReducer } = login.actions;

export const store = configureStore({
  reducer: {
    settings: slice.reducer,
    credit: credit.reducer,
    storeDetail: storeDetail.reducer,
    campaignDetails: campaignDetails.reducer,
    campaignAdd: campaignAdd.reducer,
    paymentDetail: paymentDetail.reducer,
    loginDetail: login.reducer
  }
})

// store.subscribe(() => console.log(store.getState()+"state"))