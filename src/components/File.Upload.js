import React from 'react';
import { getCurrentUser } from '../service config/auth.service';
import { FILE_UPLOAD } from '../service config/http.service';
import LinearProgress from '@mui/material/LinearProgress';
import {Box,Typography}from "@material-ui/core";
import { ToastContainer, toast } from "react-toastify";

//this is HOC for file uploading module


export default function FileUploadHoc({children, fileType, set}){
    const [progress, setProgress] = React.useState(0);

    React.useEffect(() => {
        document.getElementById(fileType).onchange = upload;
    },[])

    //this function will trigger file upload
    const upload = async ({target: {files}}) => {
        console.log({files})
        //target comming from e, files from e.target.files
        let data = new FormData();
        data.append('file', files[0]);
        
        const options = {
            onUploadProgress : (progressEvent) => {
                const {loaded, total} = progressEvent;
                let percentage = Math.round((loaded * 100) / total);
                console.log(loaded, total, percentage);
                if(percentage < 99) setProgress(percentage);
            },
            'content-type' : files[0].type
        }

        //upload http api
        FILE_UPLOAD(`/user/${getCurrentUser().id}/file-upload?fileType=${fileType}`, data, options)
            .then(response => {
               setProgress(100);
                set(response, fileType);
                
            }).catch(error => {
                console.log("error"+error.response.data.msg)
                toast.error(error.response.data.payload);
                setProgress(0);
        })

    }

   

    return (
        <>
            {children}
            { (progress === 0  || progress === 100) ? null : <><Box sx={{ display: 'flex', alignItems: 'center' }}>
      <Box sx={{ width: '100%', mr: 1 }}>
        <LinearProgress variant="determinate" value={progress}/>
      </Box>
      <Box sx={{ minWidth: 35 }}>
        <Typography variant="body2" color="text.secondary">{`
        ${progress}%`}</Typography>
      </Box>
    </Box></>}
        </>
    )


}