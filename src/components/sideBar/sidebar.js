import * as React from "react";
import PropTypes from "prop-types";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import MailIcon from "@mui/icons-material/Mail";
import MenuIcon from "@mui/icons-material/Menu";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Image from "../../assets/baeon-full.jpg";
import Profile from "../../assets/logo.jpeg";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Hamburger from "hamburger-react";

import {
  makeStyles,
  Card,
  Grid,
  Button,
  TextField,
  Select,
  MenuItem,
  FormLabel,
  TextareaAutosize,
  Input,
  FormControlLabel,
  Radio,
  RadioGroup,
  createMuiTheme,
} from "@material-ui/core";
import PhotoCamera from "@mui/icons-material/PhotoCamera";
import BackupIcon from "@mui/icons-material/Backup";
import HeightIcon from "@mui/icons-material/Height";

import Stack from "@mui/material/Stack";

import { borderRadius, height, textAlign } from "@mui/system";
import Link from "@mui/material/Link";
import {
  NavLink as RouterLink,
  LinkProps as RouterLinkProps,
  MemoryRouter,
  useLocation,
  useParams,
} from "react-router-dom";
import SettingsApplicationsIcon from "@mui/icons-material/SettingsApplications";

const drawerWidth = 270;
const theme = createMuiTheme({
  overrides: {
    MuiInput: {
      underline: {
        "&:hover:not($disabled):before": {
          backgroundColor: "rgba(0, 188, 212, 0.7)",
        },
      },
    },
  },
});

const useStyles = makeStyles((theme) => ({
  image: {
    borderRadius: "3%",
  },
  caption: {
    fontSize: "0.75rem",
    lineHeight: "1rem",
    fontWeight: "bold",
    color: "#57748a",
  },
  
  btn: {
    background: "white",
    color: "blue",
    fontWeight: "700",
    "&:hover": {
      background: "blue",
      color: "white",
    },
  },
  inputfile: {
    display: "none",
  },
  
  icon: {
    transform: "rotate(180deg)",
  },
  grid: {
    padding: "25px 0",
  },
  profile: {
    width: "50px",
    height: "50px",
    borderRadius: "50%",
  },
  
  isactive: {
    color: "red",
    background: "green",
  },
}));

const SideBar = () => {
  const classes = useStyles();

  const location = useLocation();
  const menuOne = [
    {
      name: "SHARE COUPONS",
      path: "/shareCoupons",
      icon: <SettingsApplicationsIcon />,
      activeIcon: <SettingsApplicationsIcon style={{ color: "#33c8f5" }} />,
    },
  ];
  const menuTwo = [
    {
      name: "CLAIM COUPON",
      path: "/claimCoupons",
      icon: <SettingsApplicationsIcon />,
      activeIcon: <SettingsApplicationsIcon style={{ color: "#33c8f5" }} />,
    },
    {
      name: "VIEW CAMPAIGNS",
      path: "/viewCampaigns",
      icon: <SettingsApplicationsIcon />,
      activeIcon: <SettingsApplicationsIcon style={{ color: "#33c8f5" }} />,
    },
    {
      name: "CREATE NEW CAMPAIGNS",
      path: "/createCampaign",
      icon: <SettingsApplicationsIcon />,
      activeIcon: <SettingsApplicationsIcon style={{ color: "#33c8f5" }} />,
    },
  ];
  const menuThree = [
    {
      name: "CREATE STORE",
      path: "/create-store",
      icon: <SettingsApplicationsIcon />,
      activeIcon: <SettingsApplicationsIcon style={{ color: "#33c8f5" }} />,
    },
    {
      name: "STORES",
      path: "/store",
      icon: <SettingsApplicationsIcon />,
      activeIcon: <SettingsApplicationsIcon style={{ color: "#33c8f5" }} />,
    },
    {
      name: "BUSINESS",
      path: "/business",
      icon: <SettingsApplicationsIcon />,
      activeIcon: <SettingsApplicationsIcon style={{ color: "#33c8f5" }} />,
    },
    {
      name: "REFER & EARN",
      path: "/credit",
      icon: <SettingsApplicationsIcon />,
      activeIcon: <SettingsApplicationsIcon style={{ color: "#33c8f5" }} />,
    },
    {
      name: "OUR CLIENTS",
      path: "/primeCustomer",
      icon: <SettingsApplicationsIcon />,
      activeIcon: <SettingsApplicationsIcon style={{ color: "#33c8f5" }} />,
    },
  ];

  return (
    <>
      <div>
        <Toolbar align="center" sx={{ my: 2 }}>
          <Box sx={{ width: "100%" }}>
            <img
              src={Image}
              alt="logo"
              height="100%"
              width="100%"
              className={classes.image}
            />
          </Box>
        </Toolbar>
        <Divider />
        <Box sx={{ width: "100%", px: 1, py: 2, m: 0 }}>
          <Typography
            variant="overline"
            display="block"
            sx={{
               fontSize: "0.75rem",
              lineHeight: "1rem",
              fontWeight: "bold",
              color: "#57748a",
              padding: "10px",
            }}
          >
            MEDIA PARTNER
          </Typography>
          <List>
            {menuOne.map((text, index) => (
              <Link
                component={RouterLink}
                to={`${text.path}`}
                underline="none"
                // activeClassName={classes.isactive}
              >
                <ListItem
                  button
                  key={text}
                  sx={{
                    fontSize: "0.75rem",
                    lineHeight: "1rem",
                    fontWeight: "700",
                    color: "#1c1c1c",
                    "&&.Mui-selected": {
                      backgroundColor: "white",
                      color: "#33c8f5",
                    },
                  }}
                  selected={text.path === location.pathname}
                >
                  <ListItemIcon sx={{ minWidth: "30px", p: 0 }}>
                    {text.path === location.pathname
                      ? text.activeIcon
                      : text.icon}
                  </ListItemIcon>

                  {text.name}
                </ListItem>
              </Link>
            ))}
          </List>
        </Box>

        <Divider />
        <Box sx={{ width: "100%", px: 1, py: 2, m: 0 }}>
          <Typography
            variant="overline"
            display="block"
            sx={{
              fontSize: "0.75rem",
              lineHeight: "1rem",
              fontWeight: "bold",
              color: "#57748a",
              padding: "10px",
            }}
          >
            ADVERTISER
          </Typography>
          <List>
            {menuTwo.map((text, index) => (
              <Link component={RouterLink} to={`${text.path}`} underline="none">
                <ListItem
                  button
                  key={text}
                  sx={{
                    fontSize: "0.75rem",
                    lineHeight: "1rem",
                    fontWeight: "700",
                    color: "#1c1c1c",
                    "&&.Mui-selected": {
                      backgroundColor: "white",
                      color: "#33c8f5",
                    },
                  }}
                  selected={text.path === location.pathname}
                >
                  <ListItemIcon sx={{ minWidth: "30px", p: 0 }}>
                    {text.path === location.pathname
                      ? text.activeIcon
                      : text.icon}
                  </ListItemIcon>
                  {text.name}
                </ListItem>
              </Link>
            ))}
          </List>
        </Box>

        <Divider />
        <Box sx={{ width: "100%", px: 1, py: 2, m: 0 }}>
          <Typography
            variant="overline"
            display="block"
            sx={{
              fontSize: "0.75rem",
              lineHeight: "1rem",
              fontWeight: "bold",
              color: "#57748a",
              padding: "10px",
            }}
          >
            MORE...
          </Typography>
          <List>
            <List>
              {menuThree.map((text, index) => (
                <Link
                  component={RouterLink}
                  to={`${text.path}`}
                  underline="none"
                >
                  <ListItem
                    button
                    key={text}
                    sx={{
                      fontSize: "0.75rem",
                      lineHeight: "1rem",
                      fontWeight: "700",
                      color: "#1c1c1c",
                      "&&.Mui-selected": {
                        backgroundColor: "white",
                        color: "#33c8f5",
                      },
                    }}
                    selected={text.path === location.pathname}
                  >
                    <ListItemIcon sx={{ minWidth: "30px", p: 0 }}>
                      {text.path === location.pathname
                        ? text.activeIcon
                        : text.icon}
                    </ListItemIcon>

                    {text.name}
                  </ListItem>
                </Link>
              ))}
            </List>
          </List>
        </Box>
      </div>
    </>
  );
};
export default SideBar;
