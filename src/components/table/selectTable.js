import {useState} from "react";

import { DataGrid } from "@mui/x-data-grid";
import IconButton from "@material-ui/core/IconButton";
import {
  makeStyles,
  Card,
  Grid,
  Button,
  TextField,
  Select,
  MenuItem,
  FormLabel,
  TextareaAutosize,
  Input,
  FormControlLabel,
  Radio,
  RadioGroup,
  createMuiTheme,
} from "@material-ui/core";
import { addCampaignReducer } from "../../service config/configStore";

import { useSelector, useDispatch } from "react-redux";


const useStyles = makeStyles((theme) => ({
  actionBtnpay: {
    background: "#377805",
    color: "#ffff",
  },
  actionBtnPaid: {
    background: "#1eb1e3",
    color: "#ffff",
  },
}));
export default function TableSelect({ setSelected, setShowModal, rows }) {
  const classes = useStyles();
  const [selectedRows, setSelectedRows] =useState([]);

  const columns = [
    {
      headerName: "Sr. No.",
      headerAlign: "center",
      align: "center",
      width: 100,
      hidden: true,
    },
    {
      field: "logo",
      headerName: "Logo",
      width: 150,
      headerAlign: "center",
      align: "center",
      renderCell: (params) => (
        // <Link href={`/form/${params.value}`}>{params.value}</Link>
        <img src={params.value} className="w-20 h-20" />
      ),
    },
    {
      field: "mediapartner",
      headerName: "MediaPartner",
      width: 170,
      editable: true,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "domain",
      headerName: "Domain",
      width: 150,
      editable: true,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "distance",
      headerName: "Distance(Kms.)",
      type: "number",
      width: 150,
      editable: true,
      align: "center",
      headerAlign: "center",
    },

    {
      field: "converstionrate",
      headerName: "Conversion %",
      type: "number",
      width: 200,
      editable: true,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "sharingrate",
      headerName: "Sharing %",
      type: "number",
      width: 200,
      editable: true,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "fee",
      headerName: "Fee",
      type: "number",
      width: 110,
      editable: true,
      align: "center",
      headerAlign: "center",
    },
  ];

  const row = [
    {
      id: 1,
      domain: 'Pharma',
      distance: 32,
      mediapartner: 'Johnson',
      converstionrate: 12.3,
      sharingrate: 12.3,
      fee: 35,
    },
    {
      id: 2,
      domain: 'Grocery',
      distance: 20,
      mediapartner: 'Mercedes',
      converstionrate: 12.3,
      sharingrate: 12.3,
      fee: 35,
    },
    {
      id: 3,
      domain: 'Jewelry',
      distance: 15,
      mediapartner: 'Appollo',
      converstionrate: 12.3,
      sharingrate: 12.3,
      fee: 35,
    },
    {
      id: 4,
      domain: 'Food',
      distance: 33,
      mediapartner: 'Cadbury',
      converstionrate: 12.3,
      sharingrate: 12.3,
      fee: 35,
    },
    {
      id: 5,
      domain: 'Gaming',
      distance: 78,
      mediapartner: 'Mercury',
      converstionrate: 12.3,
      sharingrate: 12.3,
      fee: 35,
    },
    {
      id: 6,
      domain: 'Pharma',
      distance: 45,
      mediapartner: 'Ninad',
      converstionrate: 12.3,
      sharingrate: 12.3,
      fee: 35,
    },
    {
      id: 7,
      domain: 'Pharma',
      distance: 2,
      mediapartner: 'DHAN',
      converstionrate: 12.3,
      sharingrate: 12.3,
      fee: 35,
    },
  ];

  const onSelect = () => {
    setShowModal(true);
  };
  const dispatch = useDispatch();
 
  

  return (
    <div style={{ height: 800, background: "white" }}>
      <DataGrid
        rows={rows}
        rowHeight={100}
        columns={columns}
        pageSize={7}
        checkboxSelection
        isRowSelectable={(params) => {
            if (selectedRows.length === 0) {
              return true;
            }
            return params.row.id === selectedRows[0];
        }}
        onSelectionModelChange={(newSelection) => {
          // console.log('mpid', rows[newSelection], newSelection)
           //   setCampaign({ ...campaign, mediaPartnerId: newSelection[0] });
           dispatch(addCampaignReducer({ mediaPartnerId: newSelection[0] }));
            setSelectedRows(newSelection);
        }}
        disableSelectionOnClick
      />
    </div>
  );
}
