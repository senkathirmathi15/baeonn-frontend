import React from 'react';
import {
    makeStyles,
    Card,
    Grid,
    Button,
    TextField,
    Select,
    MenuItem,
    FormLabel,
    TextareaAutosize,
    Input,
    FormControlLabel,
    Radio,
    RadioGroup,
    createMuiTheme,
  } from "@material-ui/core";
 
  const useStyles = makeStyles((theme) => ({
   
    card: props => ({
      width: "100%",
      boxShadow: "0 3px 20px rgb(0 0 0 / 0.2)",
    //   minHeight: "100vh",
      background: props.color ? props.color : "",
      margin: "auto",
    //   marginTop: "100px",
      borderRadius: "5px",
      padding: "20px",
    }),}));

export default function WrapperCard(prop) {
    const classes = useStyles({color:prop.bg});

  return (
   <>
   <Card className={classes.card}>
   {prop.children}
   </Card>
   </>
  )
}
