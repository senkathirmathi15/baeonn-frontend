import React from 'react';
import LoadingButton from '@mui/lab/LoadingButton';

export default function LoadingBtn({name,size,onClick,loading=false,loadingIndicator="Loading...",variant="outlined",type,disabled}) {
  return (
    <>
         <LoadingButton
          size={size}
          onClick={onClick}
          loading={loading}
          loadingIndicator={loadingIndicator}
          variant={variant}
          type={type}
          fullWidth
          disabled={disabled}
        >
          {name}
        </LoadingButton>
    </>
  )
}
