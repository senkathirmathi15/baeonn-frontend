import { DataGrid } from "@mui/x-data-grid";
import IconButton from "@material-ui/core/IconButton";
import {
  makeStyles,
  Card,
  Grid,
  Button,
  TextField,
  Select,
  MenuItem,
  FormLabel,
  TextareaAutosize,
  Input,
  FormControlLabel,
  Radio,
  RadioGroup,
  createMuiTheme,
  Avatar,
  Box,
} from "@material-ui/core";
import {useNavigate} from 'react-router-dom';
import WrapperCard from "../wrapperCard/wrapperCard";
import Logo from "../../assets/baeon-full.jpg"


const useStyles = makeStyles((theme) => ({
  actionBtnpay: {
    background: "#377805",
    color: "#ffff",
  },
  actionBtnPaid: {
    background: "#1eb1e3",
    color: "#ffff",
  },
  storeImg :{
    width: "150px",
    height:"150px",
    borderRadius:"50%"

  }
}));
export default function CardShareCoupons({ setSelected, setShowModal, rows }) {
      const classes = useStyles();
      const navigate = useNavigate();


      const onPay = (params) => {
        navigate(`/campaignPayment?id=${params}`);
      }
  const columns = [
    {
      field: "sno",
      headerName: "SNo",
      headerAlign: "center",
      align: "center",
      width: 100,
      hidden: true,
    },
    {
      field: "created",
      headerName: "created",
      width: 150,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "title",
      headerName: "Title",
      width: 150,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "description",
      headerName: "Description",
      width: 400,
      align: "left",
      headerAlign: "left",
    },
    {
      field: "numberOfCoupons",
      headerName: "No Of Coupons",
      type: "number",
      width: 120,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "shared",
      headerName: "Shared",
      type: "number",
      width: 120,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "notshared",
      headerName: "Not shared",
      type: "number",
      width: 120,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "paymentstatus",
      headerName: "Payment Status",
      width: 150,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "action",
      headerName: "Action",
      width: 150,
      align: "center",
      headerAlign: "center",
      renderCell: (params) => {
        return (
          <div>
            {params.row.paymentstatus === "pending" ? (
              <Button
                color="success"
                size="small"
                className={classes.actionBtnpay}
                onClick={() => onPay(params)}
              >
                pay
              </Button>
            ) : (
              <Button
                color="success"
                size="small"
                className={classes.actionBtnPaid}
              >
                paid
              </Button>
            )}
          </div>
        );
      },
    },
  ];

 

  const onSelect = (data) => {
    const { id: campaignId } = data.row;
    console.log("data => ", data);
    setSelected(data.row);
    setShowModal(true);
  };

  return (
    <div style={{ minHeight: 800,background:"white" ,padding:"10px"}}>

      <Box component={"h2"} sx={{textAlign:"center"}}> View Campaigns</Box>
       <Grid container spacing={2} columns={{ xs: 2, md: 12 }}>
     
     
     {rows.map((item)=>{    
      return(  <>
       <Grid item xs={12} md={6}>
            <WrapperCard bg="">
          
          <Box sx={{textAlign:"center"}}>
            <img className={classes.storeImg} src={Logo}/>
          </Box>
            <Grid container spacing={2} columns={{ xs: 2, md: 12 }}>

            <Grid item xs={12} md={12}>
            Created date:{item.created}
              </Grid>
            <Grid item xs={12} md={6}>
            Title:{item.title}
              </Grid>
            <Grid item xs={12} md={6}>
            Description:{item.description}
              </Grid>
            <Grid item xs={12} md={6}>
            Number of coupons created:{item.numberOfCoupons}
              </Grid>
            <Grid item xs={12} md={6}>
            Number of coupons shared:{item.shared}
              </Grid>
            <Grid item xs={12} md={12}>
            Status (Active, Ended):{item.paymentstatus}      
                    </Grid>
            <Grid item xs={12} md={12}>
            Action- Duplicate:{item.paymentstatus}      
                    </Grid>
              </Grid>
              <Box sx={{textAlign:"center"}}>
              {/* <Button
            color="success"
            size="small"
            onClick={() => onSelect(item.id)}
            className={classes.actionBtnShare}
          >
            Share
          </Button> */}
           {item.paymentstatus === "pending" ? (
              <Button
                color="success"
                size="small"
                className={classes.actionBtnpay}
                onClick={() => onPay(item.id)}
              >
                pay
              </Button>
            ) : (
              <Button
                color="success"
                size="small"
                className={classes.actionBtnPaid}
              >
                paid
              </Button>
            )}
              </Box>
             
            </WrapperCard>
          </Grid>
          </>)
          })}

        
        </Grid>

      {/* <DataGrid rows={rows} rowHeight={100} columns={columns} pageSize={7} /> */}
    </div>
  );
}
