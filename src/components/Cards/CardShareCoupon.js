import { DataGrid } from "@mui/x-data-grid";
import IconButton from "@material-ui/core/IconButton";
import {
  makeStyles,
  Card,
  Grid,
  Button,
  TextField,
  Select,
  MenuItem,
  FormLabel,
  TextareaAutosize,
  Input,
  FormControlLabel,
  Radio,
  RadioGroup,
  createMuiTheme,
  Box,
} from "@material-ui/core";
import {
  shareCoupon
  
} from "../../service config/admin.service";
import { ToastContainer, toast } from "react-toastify";
import WrapperCard from "../wrapperCard/wrapperCard";

const useStyles = makeStyles((theme) => ({
 
  actionBtnShare: {
    background: "#377805",
    color: "#ffff",
    "&:hover": {
      background: "blue",
      color: "white",
    },
  },
  
}));
export default function CardShareCoupons({ setSelected, setShowModal, rows }) {
  const classes = useStyles();

  

  const columns = [
    {
      field: "id",
      headerName: "Id",
      headerAlign: "center",
      align: "center",
      width: 100,
      hidden: true,
    },
    {
      field: "created",
      headerName: "created",
      width: 150,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "title",
      headerName: "Title",
      width: 150,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "description",
      headerName: "Description",
      width: 400,
      align: "left",
      headerAlign: "left",
    },
    {
      field: "numberOfCoupons",
      headerName: "No Of Coupons",
      type: "number",
      width: 120,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "shared",
      headerName: "Shared",
      type: "number",
      width: 120,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "notshared",
      headerName: "Not shared",
      type: "number",
      width: 120,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "status",
      headerName: "status",
      width: 150,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "action",
      headerName: "Action",
      width: 150,
      align: "center",
      headerAlign: "center",
      renderCell: (params) => {
        return (
          //   <button
          //     // onClick={() => onSelect(params)}
          //     className="bg-lightBlue-600 text-white font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 ease-linear transition-all duration-150"
          //     type="button"
          //   >
          //     Share
          //   </button>
          <Button
            color="success"
            size="small"
            onClick={() => onSelect(params)}
            className={classes.actionBtnpay}
          >
            Share
          </Button>
        );
      },
    },
  ];

  const onSelect = (data) => {
    setShowModal(true);
    setSelected(data)
  };
  

  return (
    <div style={{  background: "white",padding:"20px" }}>
      <Box component={"h2"} sx={{textAlign:"center"}}> share coupon</Box>
      <Grid container spacing={2} columns={{ xs: 2, md: 12 }}>
     
     
     {rows.map((item)=>{    
      return(  <>
       <Grid item xs={12} md={6}>
            <WrapperCard bg="">
            <Grid container spacing={2} columns={{ xs: 2, md: 12 }}>

            <Grid item xs={12} md={12}>
            Advertiser Name:{item.id}
              </Grid>
            <Grid item xs={12} md={6}>
            Title:{item.title}
              </Grid>
              <Grid item xs={12} md={6}>
            Conversion rate:{item.paymentstatus}      
                    </Grid>
           
            <Grid item xs={12} md={6}>
            Total Number of coupons:{item.numberOfCoupons}
              </Grid>
            <Grid item xs={12} md={6}>
            Number of coupons shared:{item.shared}
              </Grid>
          
                    <Grid item xs={12} md={12}>
            Description:{item.description}
              </Grid>
              </Grid>
              <Box sx={{textAlign:"center"}}>
              <Button
            color="success"
            size="small"
            onClick={() => onSelect(item.id)}
            className={classes.actionBtnShare}
          >
            Share
          </Button>
              </Box>
             
            </WrapperCard>
          </Grid>
          </>)
          })}

        
        </Grid>
     
      {/* <DataGrid rows={rows} rowHeight={100} columns={columns} pageSize={7} /> */}
    </div>
  );
}
