import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { useSelector, useDispatch } from "react-redux";
import { bankDetailReducer } from "../../service config/configStore";
import FileUploadHoc from "../File.Upload";
import { ToastContainer, toast } from "react-toastify";
import {
  submitBankDetail,
  getBankDetail,
} from "../../service config/admin.service";

import {
  makeStyles,
  Grid,
  Box,
  IconButton,
  TextField,
  Select,
  MenuItem,
  FormLabel,
  TextareaAutosize,
  Input,
  FormControlLabel,
  Radio,
  RadioGroup,
  createMuiTheme,
} from "@material-ui/core";
import BackupIcon from "@mui/icons-material/Backup";
import Lable from "../Typography/lable";


const theme = createMuiTheme({
  overrides: {
    MuiInput: {
      underline: {
        "&:hover:not($disabled):before": {
          backgroundColor: "rgba(0, 188, 212, 0.7)",
        },
      },
    },
  },
});

const useStyles = makeStyles((theme) => ({
  lableTitle: {
    fontSize: "0.75rem",
    lineHeight: "1rem",
    fontWeight: "bold",
    color: "#57748a",
  },
  lable: {
    fontSize: "0.75rem",
    lineHeight: "1rem",
    fontWeight: "bold",
    color: "#57748a",
  },
  cards: {
    marginTop: "100px",
  },

  card: {
    boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
    borderRadius: "5px",
    padding: "20px",
  },
  canc: {
    background: "blue",
    color: "white",
    fontWeight: "700",
    "&:hover": {
      background: "blue",
      color: "white",
    },
  },
  inputfile: {
    display: "none",
  },
  upload: {
    background: "#f0f3f5",
    display: "flex",
    flexDirection: "column",
    width: "100%",
    height: "100%",
    justifyContent: "center",
  },
  radio: {
    "&$checked": {
      color: "#4B8DF8",
    },
  },
  checked: {},
}));

export default function CardBankDetails() {
  const classes = useStyles();
  const bankDetail = useSelector((state) => state.settings.bankDetail);
  const dispatch = useDispatch();
  const setimageUrl = (data) => {
    dispatch(bankDetailReducer({ kycDocument: data.data.payload.Location }));
  };
  React.useEffect(() => {
    getBankDetail();
  }, []);
  const handleSubmit = async (event) => {
    event.preventDefault();
    const res = await submitBankDetail(bankDetail);
    if (!res.error) {
      toast.success(res.payload);
    } else {
      toast.error(res.payload);
    }
  };

  return (
    <>
      <Card>
        <CardContent>
          <form onSubmit={handleSubmit}>
          <Grid
            container
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
            className={classes.grid}
          >
            <Grid item xs={6}>
              <Typography
                variant="h6"
                display="block"
                sx={{
                  // fontSize: "12px",
                  lineHeight: "1rem",
                  fontWeight: "bold",
                  color: "#1c1c1c",
                }}
                className={classes.lable}
              >
                Business Details
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Box sx={{ textAlign: "end" }}>
              <Button
                    size="small"
                    type="submit"
                    sx={{
                      background: "blue",
                      color: "white",
                      fontWeight: "700",
                      "&:hover": {
                        background: "blue",
                        color: "white",
                      },
                    }}
                  >
                    update
                  </Button>
              </Box>
            </Grid>
          </Grid>{" "}
          <Box p={2}>
            <Typography
              variant="p"
              display="block"
              className={classes.lableTitle}
            >
              ACCOUNT DETAILS
            </Typography>
            <Box p={2}>
              <Grid container spacing={2}>
                <Grid item xs={6}>
                  <div>
                 
                    <Lable
                  style={{ my: 2, marginRight: "auto" }}
                  title="ACCOUNT NUMBER"
                />
                    <TextField
                      id="outlined-basic"
                      variant="outlined"
                      fullWidth
                      type="number"
                      value={bankDetail.accountNumber}

                      sx={{
                        border: "none",
                      }}
                      onChange={(e) =>
                        dispatch(
                          bankDetailReducer({
                            accountNumber: e.target.value,
                          })
                        )
                      }
                    />
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <div>
                 
                    <Lable
                  style={{ my: 2, marginRight: "auto" }}
                  title=" PAN NUMBER"
                />
                    <TextField
                      id="outlined-basic"
                      variant="outlined"
                      fullWidth
                      type="number"
                      value={bankDetail.panNumber}

                      sx={{
                        border: "none",
                      }}
                      onChange={(e) =>
                        dispatch(
                          bankDetailReducer({
                            panNumber: e.target.value,
                          })
                        )
                      }
                    />
                  </div>
                </Grid>
              </Grid>

            
              <Lable
                  style={{ my: 2, marginRight: "auto" }}
                  title=" ACCOUNT HOLDER"
                />
              <TextField
                id="outlined-basic"
                variant="outlined"
                fullWidth
                sx={{
                  border: "none",
                }}
                value={bankDetail.accountHolderName}

                onChange={(e) =>
                  dispatch(
                    bankDetailReducer({
                      accountHolderName: e.target.value,
                    })
                  )
                }
              />
              <Grid container spacing={2}>
                <Grid item xs={6}>
                  <div>
                   
                    <Lable
                  style={{ my: 2, marginRight: "auto" }}
                  title="BANK NAME"
                />
                    <TextField
                      id="outlined-basic"
                      variant="outlined"
                      fullWidth
                      value={bankDetail.bankName}

                      sx={{
                        border: "none",
                      }}
                      onChange={(e) =>
                        dispatch(
                          bankDetailReducer({
                            bankName: e.target.value,
                          })
                        )
                      }
                    />
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <div>
                  
                    <Lable
                  style={{ my: 2, marginRight: "auto" }}
                  title="IFSC CODE"
                />
                    <TextField
                      id="outlined-basic"
                      variant="outlined"
                      fullWidth
                      sx={{
                        border: "none",
                      }}
                      value={bankDetail.ifscCode}

                      onChange={(e) =>
                        dispatch(
                          bankDetailReducer({
                            ifscCode: e.target.value,
                          })
                        )
                      }
                      
                    />
                  </div>
                </Grid>
              </Grid>
              <FileUploadHoc fileType='bankstatement' set={setimageUrl}>
              <Box
                component="label"
                sx={{
                  width: "90%",
                  margin: "0 auto",
                  padding: "10px",
                  background: "white",
                  height: "100px",
                  boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                }}
                htmlFor="bankstatement"
                mt={2}
              >
                <BackupIcon
                  sx={{
                    fontSize: "40px",
                    color: "blue",
                  }}
                />
               
                <Lable
                  style={{ my: 2, textAlign:"center" }}
                  title=" Upload KYC Details"
                />
                <Input
                  accept="image/*"
                  id="bankstatement"
                  type="file"
                  className={classes.inputfile}
                />
              </Box>
              </FileUploadHoc >
              <Box sx={{ paddingTop: "10px" }}>
                  {" "}
                  {bankDetail.kycDocument ? "Last Uploaded:" : ""}{" "}
                  {bankDetail.kycDocument ? (
                    <a href={bankDetail.kycDocument} target="_blank">
                      Click here
                    </a>
                  ) : (
                    ""
                  )}
                </Box>
            
              <Lable
                  style={{ my: 2, marginRight: "auto" }}
                  title=" KYC Details include Cancelled Cheque Image or Bank Statement"
                />
            </Box>
            
          </Box>
          </form>
        </CardContent>
      </Card>
    </>
  );
}
