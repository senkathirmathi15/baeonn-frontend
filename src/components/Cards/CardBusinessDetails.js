import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { useSelector, useDispatch } from "react-redux";
import { businessDetailReducer } from "../../service config/configStore";
import FileUploadHoc from "../File.Upload";
import { ToastContainer, toast } from "react-toastify";
import {
  submitBusinessDetail,
  getBussinessDetail,
} from "../../service config/admin.service";
import Lable from "../Typography/lable";
import {
  makeStyles,
  Grid,
  Box,
  IconButton,
  TextField,
  Select,
  MenuItem,
  FormLabel,
  TextareaAutosize,
  Input,
  FormControlLabel,
  Radio,
  RadioGroup,
} from "@material-ui/core";
import BackupIcon from "@mui/icons-material/Backup";

const useStyles = makeStyles((theme) => ({
  lableTitle: {
    fontSize: "0.75rem",
    lineHeight: "1rem",
    fontWeight: "bold",
    color: "#57748a",
  },
  lable: {
    fontSize: "0.75rem",
    lineHeight: "1rem",
    fontWeight: "bold",
    color: "#57748a",
  },
  cards: {
    marginTop: "100px",
  },

  card: {
    boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
    borderRadius: "5px",
    padding: "20px",
  },
  canc: {
    background: "blue",
    color: "white",
    fontWeight: "700",
    "&:hover": {
      background: "blue",
      color: "white",
    },
  },
  inputfile: {
    display: "none",
  },
  upload: {
    background: "#f0f3f5",
    display: "flex",
    flexDirection: "column",
    width: "100%",
    height: "100%",
    justifyContent: "center",
  },
  radio: {
    "&$checked": {
      color: "#4B8DF8",
    },
  },
  checked: {},
}));

export default function CardBankDetails() {
  const classes = useStyles();
  const businessDetail = useSelector((state) => state.settings.businessDetail);
  const dispatch = useDispatch();
  const setimageUrl = (data) => {
    dispatch(businessDetailReducer({ gstUrl: data.data.payload.Location }));
  };

  React.useEffect(() => {
    getBussinessDetail();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const res = await submitBusinessDetail(businessDetail);
    if (!res.error) {
      toast.success(res.payload);
    } else {
      toast.error(res.payload);
    }
  };

  return (
    <>
      <ToastContainer></ToastContainer>
      <Card>
        <CardContent>
          <form onSubmit={handleSubmit}>
            <Grid
              container
              columnSpacing={{ xs: 1, sm: 2, md: 3 }}
              className={classes.grid}
            >
              <Grid item xs={6}>
                <Typography
                  variant="h6"
                  display="block"
                  sx={{
                    // fontSize: "12px",
                    lineHeight: "1rem",
                    fontWeight: "bold",
                    color: "#1c1c1c",
                  }}
                  className={classes.lable}
                >
                  Business Details
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <Box sx={{ textAlign: "end" }}>
                  <Button
                    size="small"
                    type="submit"
                    sx={{
                      background: "blue",
                      color: "white",
                      fontWeight: "700",
                      "&:hover": {
                        background: "blue",
                        color: "white",
                      },
                    }}
                  >
                    update
                  </Button>
                </Box>
              </Grid>
            </Grid>{" "}
            <Box p={2}>
              <Typography
                variant="p"
                display="block"
                className={classes.lableTitle}
              >
                GST DETAILS
              </Typography>
              <Box p={2}>
                <Lable
                  style={{ my: 2, marginRight: "auto" }}
                  title="IS YOUR BUSINESS GST EXEMPTED?"
                />

                <RadioGroup
                  row
                  // aria-labelledby="demo-radio-buttons-group-label"
                  defaultValue={businessDetail.gstExempted ? "yes" : "no"}
                  name="radio-buttons-group"
                  onChange={(e) =>
                    dispatch(
                      businessDetailReducer({
                        gstExempted: e.target.value === "yes" ? true : false,
                      })
                    )
                  }
                >
                  <FormControlLabel
                    value="yes"
                    control={
                      <Radio
                        classes={{
                          root: classes.radio,
                          checked: classes.checked,
                        }}
                      />
                    }
                    label="Yes"
                  />
                  <FormControlLabel
                    value="no"
                    control={
                      <Radio
                        classes={{
                          root: classes.radio,
                          checked: classes.checked,
                        }}
                      />
                    }
                    // onClick={()=>dispatch(businessDetailReducer({gstExempted:false}))}

                    label="No"
                  />
                </RadioGroup>
                <div>
                  <Lable style={{ my: 2, marginRight: "auto" }} title="STATE" />

                  <Select
                    value={businessDetail.state}
                    // onChange={handleChange}
                    onChange={(e) =>
                      dispatch(businessDetailReducer({ state: e.target.value }))
                    }
                    displayEmpty
                    inputProps={{ "aria-label": "Without label" }}
                    fullWidth
                    variant="outlined"
                  >
                    <MenuItem value="">
                      <em>Select state</em>
                    </MenuItem>
                    {["Maharastra", "Tamil Nadu"].map((state) => (
                      <MenuItem key={state} value={state}>
                        {state}
                      </MenuItem>
                    ))}
                  </Select>
                </div>

                <Lable
                  style={{ my: 2, marginRight: "auto" }}
                  title="GST IDENTIFICATION NUMBER"
                />

                <TextField
                  id="outlined-basic"
                  variant="outlined"
                  fullWidth
                  type="text"
                  sx={{
                    border: "none",
                  }}
                  value={businessDetail.gstNumber}
                  onChange={(e) =>
                    dispatch(
                      businessDetailReducer({ gstNumber: e.target.value })
                    )
                  }
                />
                <FileUploadHoc fileType="gstdocument" set={setimageUrl}>
                  <Box
                    component="label"
                    sx={{
                      width: "90%",
                      margin: "0 auto",
                      padding: "10px",
                      background: "white",
                      height: "100px",
                      boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                    htmlFor="gstdocument"
                    mt={2}
                  >
                    <BackupIcon
                      sx={{
                        fontSize: "40px",
                        color: "blue",
                      }}
                    />

                    <Lable
                      style={{ my: 2, textAlign: "center" }}
                      title=" Upload Acknowledgement Copy"
                    />

                    <Input
                      accept="image/*"
                      id="gstdocument"
                      type="file"
                      className={classes.inputfile}
                    />
                  </Box>
                </FileUploadHoc>
                <Box sx={{ paddingTop: "10px" }}>
                  {" "}
                  {businessDetail.gstUrl ? "Last Uploaded:" : ""}{" "}
                  {businessDetail.gstUrl ? (
                    <a href={businessDetail.gstUrl} target="_blank">
                      Click here
                    </a>
                  ) : (
                    ""
                  )}
                </Box>
              </Box>
            </Box>
          </form>
        </CardContent>
      </Card>
    </>
  );
}
