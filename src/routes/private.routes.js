import React from 'react';
import { Route, Redirect,Navigate,Outlet } from 'react-router-dom';
import {isAuthenticated} from "../service config/auth.service"

export default function PrivateRoutes() {
    return( true ? <Outlet /> : <Navigate to="/" />
    )
}


