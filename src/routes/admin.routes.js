import React from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Redirect,
} from "react-router-dom";

import BusinessDetail from "../views/auth/businessDetail";
import Store from "../views/admin/store";
import Business from "../views/admin/business";
import ViewCampaigns from "../views/admin/viewCampaigns";
import ViewCoupon from "../views/admin/shareCoupon";
import CreateCampaign from "../views/admin/createCampaign";
import ClaimCoupons from "../views/admin/claimCoupon";
import ShareCoupons from "../views/admin/shareCoupon";
import CampaignPayment from "../views/admin/campaignPayment";
import CreditAndRef from "../views/admin/creditandref";
import PrimeCustomer from "../views/admin/primeCustomer";
import Dashboard from "../views/admin/dashboard";
import BulkInvite from "../views/admin/bulkInvite";
import AddStore from "../views/admin/addStore";
import ViewStore from "../views/admin/viewStores";

const component = [
  { id: 1, path: "/create-store", element: <Dashboard /> },
  { id: 1, path: "/create-store/add-store", element: <AddStore /> },
  { id: 2, path: "/bulkinvite", element: <BulkInvite /> },
  { id: 3, path: "/store", element: <ViewStore /> },
  { id: 4, path: "/business", element: <Business /> },
  { id: 5, path: "/viewCampaigns", element: <ViewCampaigns /> },
  { id: 6, path: "/coupon", element: <ViewCoupon /> },
  { id: 7, path: "/createCampaign", element: <CreateCampaign /> },
  { id: 8, path: "/claimCoupons", element: <ClaimCoupons /> },
  { id: 9, path: "/shareCoupons", element: <ShareCoupons /> },
  { id: 10, path: "/campaignPayment", element: <CampaignPayment /> },
  { id: 11, path: "/credit", element: <CreditAndRef /> },
  {id: 12, path: "/primeCustomer",element: <PrimeCustomer />, },
  {id: 12, path: "/info",element: <BusinessDetail />, },
];

export default component;
